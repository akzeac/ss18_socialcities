// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class SocialCitiesAR : ModuleRules
{
	public SocialCitiesAR(ReadOnlyTargetRules Target) : base(Target)
	{
        //Definitions.Add("_DISABLE_EXTENDED_ALIGNED_STORAGE=1");
        //PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "AndroidStuff", "OnlineSubsystem", "OnlineSubsystemNull", "OnlineSubsystemUtils"});

		PrivateDependencyModuleNames.AddRange(new string[] { "OnlineSubsystem" });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
