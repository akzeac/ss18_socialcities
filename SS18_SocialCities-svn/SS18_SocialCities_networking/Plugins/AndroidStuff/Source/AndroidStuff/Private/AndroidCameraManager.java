package com.epicgames.ue4;

import android.app.Activity;
import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Size;
import android.view.Surface;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static android.hardware.camera2.CameraMetadata.CONTROL_SCENE_MODE_BARCODE;
import static java.lang.System.currentTimeMillis;

/**
 * Created by telepresence on 17.02.2017.
 */

public abstract class AndroidCameraManager extends HasLogger {
    protected Activity activity;
    protected HandlerThread cameraThread;
    protected Handler cameraHandler;
    protected boolean firstPicLogged;
    protected int recordedFrames;
    protected long lastReferenceTime;
    protected Size[] previewSizes;
    protected Size previewSize;
    protected boolean paused;
    protected float exposure_time;

    private static final int ReportPeriod = 1000;
    private static final int CppCameraBuffers = 4;
    protected static final int CameraFPS = 60;

    public AndroidCameraManager(Activity activity_) {
        activity = activity_;
        cameraThread = new HandlerThread("proj:camera");
        cameraThread.start();
        cameraHandler = new Handler(cameraThread.getLooper());
        paused = false;

        firstPicLogged = false;
        lastReferenceTime = currentTimeMillis();
        recordedFrames = 0;

        exposure_time = 10;

        cameraHandler.postDelayed(new Runnable() {
            public void run() {
                in_logFPS();
                cameraHandler.postDelayed(this, ReportPeriod);
            }
        }, ReportPeriod);
    }

    protected void in_logFPS() {
        long actualReferenceTime = currentTimeMillis();
        int deltaTime = (int)(actualReferenceTime - lastReferenceTime);
        if (deltaTime != 0) {
            int fps = ReportPeriod * recordedFrames / deltaTime;
            Log.v("Camera FPS: " + fps);
        }
        recordedFrames = 0;
        lastReferenceTime = actualReferenceTime;
    }

    public int[] setCameraParameters(int width, int height, float exposure_time_) {
        Log.d("Requesting camera size of " + width + "x" + height + ".");
        if (previewSizes == null || width < 0 || height < 0) {
            Log.e("Cannot set camera size, it has not been setup yet!");
            return new int[] {0, 0};
        }

        int requestedArea = width * height;
        long nearestAreaDiff = Long.MAX_VALUE;
        Size nearestSize = null;

        for (Size size : previewSizes) {
            if (size.getWidth() > width || size.getHeight() > height) {
                //Log.d("Rejecting " + size.width + "x" + size.height + ".1");
                continue;
            }
            long area = (long)size.getWidth() * (long)size.getHeight();
            long areaDiff = requestedArea - area;
            if (areaDiff < nearestAreaDiff) {
                nearestSize = size;
                nearestAreaDiff = areaDiff;
            } else {
                //Log.d("Rejecting " + size.width + "x" + size.height + ".2");
            }
        }

        previewSize = nearestSize;
        exposure_time = exposure_time_;
        Log.d("Camera size has been set to " + previewSize.getWidth() + "x" + previewSize.getHeight() + ".");
        return new int[] {previewSize.getWidth(), previewSize.getHeight()};
    }

    public abstract boolean setup(int facing);

    public abstract void open();

    public void setActivityState(AndroidManager.State state) {
        switch(state) {
            case ACTIVE:
                paused = false;
                break;
        }
    }
}

class CameraManagerAPI2 extends AndroidCameraManager {
    private enum State {
        UNINITIALIZED,
        SETUP,
        START_OPEN,
        PERMISSION_GRANTED,
        ON_OPENED,
        HAS_SESSION,
        CAPTURING,
        ERROR
    }
    private State state;
    private CameraManager manager;
    private String cameraId;
    private CameraDevice camera;
    private ImageReader reader;
    private CameraCaptureSession session;
    private CaptureRequest request;
    private CameraCharacteristics characteristics;
    private int[] tempData;
    private long min_exposure_range;

    public CameraManagerAPI2(Activity activity_) {
        super(activity_);
        Log.d("Using camera API 2.");
        tempData = new int[5];
        changeState(State.UNINITIALIZED);
    }

    @Override
    public boolean setup(final int facing) {
        final Object lock = new Object();
        cameraHandler.post(new Runnable() {
            public void run() {
                in_setup(facing == 0 ?
                        CameraCharacteristics.LENS_FACING_FRONT :
                        CameraCharacteristics.LENS_FACING_BACK);
                synchronized (lock) {
                    lock.notify(); // notify outside
                }
            }
        });
        synchronized (lock) { // wait until notified
            try {
                lock.wait();
            } catch (InterruptedException e) {
            }
        }
        return cameraId != null;
    }

    private void in_setup(int req_facing) {
        Log.d("Setting up camera...");
        if (state != State.UNINITIALIZED) {
            Log.d("Camera is already setup!");
            return;
        }
        manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            characteristics = null;
            String[] cameraIds = manager.getCameraIdList();
            cameraId = null;
            for (String cameraId_ : cameraIds) {
                characteristics = manager.getCameraCharacteristics(cameraId_);
                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing != null && facing == req_facing) {
                    cameraId = cameraId_;
                    break;
                }
            }
            if (cameraId == null) {
                Log.d("Camera setup() failed!");
                changeState(State.ERROR);
            } else {
                Log.d("Camera set up.");
                changeState(State.SETUP);
                StreamConfigurationMap streamConfigurationMap = characteristics.get(
                        CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                previewSizes = streamConfigurationMap.getOutputSizes(ImageFormat.YUV_420_888);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void changeState(final State newState) {
        cameraHandler.post(new Runnable() {
            public void run() {
                Log.d("Changing camera state to " + newState);
                state = newState;
                onStateChanged();
            }
        });
    }

    @Override
    public void open() {
        cameraHandler.post(new Runnable() {
            public void run() {
                if (previewSize == null || previewSize.getWidth() == 0 || previewSize.getHeight() == 0) {
                    Log.e("Camera size has not been set!");
                    changeState(State.ERROR);
                } else {
                    changeState(State.START_OPEN);
                }
            }
        });
    }

    private void onStateChanged() {
        switch (state) {
            case START_OPEN:
                // lala
                changeState(State.PERMISSION_GRANTED);
                break;
            case PERMISSION_GRANTED:
                try {
                    Log.d("Trying to open camera...");
                    manager.openCamera(cameraId, new CameraDevice.StateCallback() {
                        @Override
                        public void onOpened(@NonNull CameraDevice camera_) {
                            camera = camera_;
                            changeState(State.ON_OPENED);
                        }

                        @Override
                        public void onDisconnected(@NonNull CameraDevice camera_) {
                            camera = null;
                            changeState(State.ERROR);
                        }

                        @Override
                        public void onError(@NonNull CameraDevice camera_, int error) {
                            camera = null;
                            changeState(State.ERROR);
                        }
                    }, null);

                } catch (CameraAccessException e) {
                    e.printStackTrace();
                    changeState(State.ERROR);
                }
                break;
            case ON_OPENED:
                reader = ImageReader.newInstance(
                        previewSize.getWidth(),
                        previewSize.getHeight(),
                        ImageFormat.YUV_420_888,
                        2
                );
                List<Surface> surfaces = Collections.singletonList(reader.getSurface());
                try {
                    camera.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session_) {
                            session = session_;
                            changeState(State.HAS_SESSION);
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session_) {
                            session = null;
                            changeState(State.ERROR);
                        }
                    }, null);

                } catch (CameraAccessException e) {
                    e.printStackTrace();
                    changeState(State.ERROR);
                }
                break;
            case HAS_SESSION:
                try {
                    CaptureRequest.Builder builder = camera.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
                    if (exposure_time != 0) {
                        StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                        long minFrameDuration = map.getOutputMinFrameDuration(ImageFormat.YUV_420_888, previewSize);
                        builder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_OFF);
                        builder.set(CaptureRequest.SENSOR_FRAME_DURATION, minFrameDuration);

                        long time_in_ns = (long) (exposure_time * 1e6);
                        builder.set(CaptureRequest.SENSOR_EXPOSURE_TIME, time_in_ns);
                        Log.d("Setting exposure time to " + exposure_time);
                        Log.d("Setting frame duration to " + minFrameDuration);
                    }
                    builder.addTarget(reader.getSurface());
                    request = builder.build();

                    reader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
                        @Override
                        public void onImageAvailable(ImageReader reader) {
                            if (paused) {
                                return;
                            }
                            Image image = reader.acquireLatestImage();
                            if (image == null) {
                                return;
                            }
                            Image.Plane[] planes = image.getPlanes();
                            tempData[0] = image.getWidth();
                            tempData[1] = image.getHeight();
                            tempData[2] = planes[0].getRowStride();
                            tempData[3] = planes[1].getRowStride();
                            tempData[4] = planes[2].getRowStride();
                            in_onPreviewFrame(
                                    planes[0].getBuffer(),
                                    planes[1].getBuffer(),
                                    planes[2].getBuffer()
                            );
                            image.close();
                        }
                    }, null);
                    session.setRepeatingRequest(request, null, null);
                    changeState(State.CAPTURING);
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                    changeState(State.ERROR);
                }
            case CAPTURING:
                Log.d("Capturing!");
                break;
            case ERROR:
                Log.d("Error! Stopping here.");
                break;
        }
    }

    private void in_onPreviewFrame(ByteBuffer y, ByteBuffer u, ByteBuffer v) {
        if (!firstPicLogged) {
            Log.d("Receiving camera images...");
            firstPicLogged = true;
        }
        recordedFrames++;
        nativeUpdateTexture(y, u, v, tempData);
    }

    private native void nativeUpdateTexture(ByteBuffer y, ByteBuffer u, ByteBuffer v, int[] tempData);
}


class CameraManagerAPI1 extends AndroidCameraManager {
    private Camera camera;
    private SurfaceTexture texture;
    private byte[] bufferA;
    private byte[] bufferB;

    CameraManagerAPI1(Activity activity_) {
        super(activity_);
        Log.d("Using camera API 1.");
    }

    @Override
    public boolean setup(final int facing) {
        final Object lock = new Object();
        cameraHandler.post(new Runnable() {
            public void run() {
                in_setup(facing == 0 ?
                        Camera.CameraInfo.CAMERA_FACING_FRONT :
                        Camera.CameraInfo.CAMERA_FACING_BACK);
                synchronized (lock) {
                    lock.notify(); // notify outside
                }
            }
        });
        synchronized (lock) { // wait until notified
            try { lock.wait(); } catch (InterruptedException e) { }
        }
        return camera != null;
    }

    private void in_setup(int facing) {
        Log.d("Setting up camera...");
        if (camera != null) {
            Log.d("Camera is already setup!");
        } else {
            texture = new SurfaceTexture(0);
            try {
                Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
                    Camera.getCameraInfo(i, cameraInfo);
                    if (cameraInfo.facing == facing) {
                        camera = Camera.open(i);
                        break;
                    }
                }
                if (camera == null) {
                    Log.e("Cannot find camera with facing " + facing);
                    return;
                }
                Camera.Parameters parameters = camera.getParameters();
                List<Camera.Size> previewSizes_ = parameters.getSupportedPreviewSizes();
                previewSizes = new Size[previewSizes_.size()];
                for (int i = 0; i < previewSizes_.size(); i++) {
                    Camera.Size size_ = previewSizes_.get(i);
                    previewSizes[i] = new Size(size_.width, size_.height);
                }
            } catch(RuntimeException e) {
                e.printStackTrace();
            }
            if (camera == null) {
                Log.d("Camera.open() failed!");
            } else {
                Log.d("Camera set up.");
            }
        }
    }

    public void open() {
        cameraHandler.post(new Runnable() {
            public void run() {
                in_open();
            }
        });
    }

    private void in_open() {
        Log.d("Opening camera...");
        if (camera == null) {
            Log.e("Cannot open camera, setup has not been called!");
            return;
        }
        if (previewSize == null || previewSize.getWidth() == 0 || previewSize.getHeight() == 0) {
            Log.e("Cannot open camera, size has not been set!");
            return;
        }
        Camera.Parameters parameters = camera.getParameters();
        parameters.setPreviewFormat(ImageFormat.NV21);
        parameters.setPreviewSize(previewSize.getWidth(), previewSize.getHeight());
        parameters.setRecordingHint(true);
        camera.setParameters(parameters);

        int previewLength = previewSize.getWidth() * previewSize.getHeight() * 3 / 2;
        if (bufferA == null || previewLength != bufferA.length) {
            bufferA = new byte[previewLength];
            bufferB = new byte[previewLength];
        }
        camera.addCallbackBuffer(bufferA);
        camera.addCallbackBuffer(bufferB);
        camera.setPreviewCallbackWithBuffer(new Camera.PreviewCallback() {
            public void onPreviewFrame(byte[] data, Camera camera) {
                in_onPreviewFrame(data);
            }
        });
        try {
            camera.setPreviewTexture(texture);
        } catch (IOException e) {
            e.printStackTrace();
        }
        camera.startPreview();
        Log.d("Camera opened.");
    }

    private void in_onPreviewFrame(byte[] data) {
        if (!firstPicLogged) {
            Log.d("Receiving camera images...");
            firstPicLogged = true;

            if (exposure_time != 0) {
                Camera.Parameters parameters = camera.getParameters();
                parameters.setAutoExposureLock(true);
                parameters.setExposureCompensation(
                        (int) (exposure_time / parameters.getExposureCompensationStep()));
                Log.d("Setting exposure value to " + exposure_time);
                camera.setParameters(parameters);
            }
        }
        recordedFrames++;
        int height = previewSize.getHeight();
        int width = previewSize.getWidth();
        nativeUpdateTexture(data, width, height);
        camera.addCallbackBuffer(data);
    }

    private native void nativeUpdateTexture(byte[] ptr, int width, int height);
};



