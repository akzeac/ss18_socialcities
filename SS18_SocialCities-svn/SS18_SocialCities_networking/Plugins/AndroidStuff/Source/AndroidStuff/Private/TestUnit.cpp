#include "AndroidStuffPCH.h"
#include "AndroidManager.h"
#include "Activity.h"
#include "TestUnit.h"
#include "Camera.h"

TestUnit::TestUnit() {
	cameraOn = false;
	sensorsOn = false;
	should_stop = false;
}

bool TestUnit::initialize(const Values &values) {
	if (AndroidActivity::IsAndroid()) {
		return true;
	}

	basePath = values.getString("cam_sample_path", "");
	std::string configPath = basePath + "capture.dat";

	sds.clear();
	bool success = Markers::SensorDataReader::read(configPath, sds);
	if (!success) {
		Log::d(str("TestUnit: Cannot find sensor data file '", configPath, "'!"));
		return false;
	}
	if (sds.empty()) {
		Log::d(str("TestUnit: Sensor file is empty!"));
	}

	return true;
}

void TestUnit::start() {
	if (sds.empty()) {
		return;
	}
	sds_it = sds.begin();
	start_time_real = Timer::getTicks();
	start_time_sensors = sds_it->timestamp;
	thread.post([this] { processSensorData(); });
}

void TestUnit::processImage(TimeVal timestamp) {
	int32_t width, height;
	string file = str(basePath, "image-", timestamp, ".bmp");
	bool success = Markers::RawBmpReader::info(file, width, height);
	if (!success) {
		Log::d(str("TestUnit: Failed to read image '", file, "'!"));
		return;
	}
	if (tmp_image == nullptr || tmp_image->width != width || tmp_image->height != height) {
		tmp_image = std::make_shared<Image>(width, height, 1);
	}

	success = Markers::RawBmpReader::read(file, *tmp_image);
	if (!success) {
		Log::d(str("TestUnit: Failed to read image '", file, "'!"));
		return;
	}

	CameraImage::Ptr buffer = AndroidActivity::GetCameraImage(width, height, width, width, CameraAPI::V2);
	if (buffer != nullptr) {
		std::copy(tmp_image->ptr, tmp_image->ptr + tmp_image->size, buffer->raw_ptr);
		std::fill(
			buffer->raw_ptr + tmp_image->size, 
			buffer->raw_ptr + buffer->raw_size, (uint8_t)192);
		AndroidActivity::OnCameraImageArrived(buffer);
	}
	else {
		Log::w("TestUnit: buffer is null!");
	}

}

void TestUnit::processSensorData() {
	SensorData sd = *sds_it++;
	//Log::d(str("SensorData: ", sd));
	TimeVal time_real = Timer::getTicks();
	TimeVal time_sensors = sd.timestamp;
	TimeVal time_diff = (time_sensors - start_time_sensors) - (time_real - start_time_real);
	if (time_diff > 0) {
		timer.sleepFor(time_diff);
	}
	if (should_stop) {
		Log::d("TestUnit: Interrupted. Leaving thread.");
		return;
	}
	switch (sd.type) {
	case SensorData::Type::Image:
		processImage(sd.timestamp);
		break;
	case SensorData::Type::Accelerometer:
	case SensorData::Type::Rotation_Vector:
	case SensorData::Type::Linear_Acceleration:
	case SensorData::Type::Game_Rotation_Vector:
		AndroidActivity::OnSensorDataArrived((int)sd.type, sd.data.data(), 4);
		break;
	default:
		Log::w(str("TestUnit: Spurious sensor data type ", (int) sd.type));
		break;
	}

	if (should_stop) {
		Log::d("TestUnit: Interrupted. Leaving thread.");
	}
	else if (sds_it == sds.end()) {
		Log::d("TestUnit: Finished all available tasks. Leaving thread.");
	} 
	else {
		thread.post([this] { processSensorData(); });
	}
}

void TestUnit::stop() {
	should_stop = true;
	timer.wakeUp();
	thread.join();
}

TestUnit::~TestUnit() {
	should_stop = true;
	timer.wakeUp();
}