#include "AndroidStuffPCH.h"
#include "Camera.h"


static const int Max_Images = 10;

Camera::Camera() :
	Repository(
		[this]() { 
			return container.size() > Max_Images ? 
				nullptr :
				std::make_shared<CameraImage>(width, height, y_pitch, uv_pitch, camera_api);  
		}, 
		nullptr
	),
	width(0), height(0), y_pitch(0), uv_pitch(0), camera_api(CameraAPI::Invalid) {}

CameraImage::Ptr Camera::resize(int width_, int height_, 
		int y_pitch_, int uv_pitch_, CameraAPI camera_api_, bool &resized) {
	lock_guard lock(mutex);
	if (width != width_ || height != height_) {
		width = width_;
		height = height_;
		y_pitch = y_pitch_;
		uv_pitch = uv_pitch_;
		camera_api = camera_api_;
		container.clear();

		Log::d("Resizing camera to:");
		Log::d("width: " + str(width));
		Log::d("height: " + str(height));
		Log::d("y_pitch: " + str(y_pitch));
		Log::d("uv_pitch: " + str(uv_pitch));
		resized = true;
	}
	else {
		resized = false;
	}

	TimeVal timestamp = Timer::getTicks();
	removeOld(timestamp);
	return getOrCreate(timestamp);
}

int Camera::getWidth() const {
	return width;
}

int Camera::getHeight() const {
	return height;
}

