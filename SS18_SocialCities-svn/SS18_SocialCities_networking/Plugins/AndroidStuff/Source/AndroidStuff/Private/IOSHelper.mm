//
//  IOSHelper.m
//  SocialCities_Index
//
//  Created by Antonio Zea on 19.06.18.
//  Copyright © 2018 ISAS, Inc. All rights reserved.
//

#include "IOSHelper.h"

#if defined(__APPLE__)
#import <Foundation/Foundation.h>
#import "TargetConditionals.h"

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface Controller : UIViewController<WKNavigationDelegate, WKUIDelegate>
{
    size_t width;
    size_t height;
    uint8_t* data;
    CGRect frameRect;
    WKWebView *webView;
    IOS_Callback callback;
    int tag;
    int managerId;
    int scale;
}
@end

static Controller *controllers[3];

@implementation Controller

-(Controller*)init {
    width = 0;
    height = 0;
    data = nil;
    webView = nil;
    callback = nil;
    tag = -1;
    managerId = -1;
    scale = [[UIScreen mainScreen] scale];
    return self;
}

+(CGContextRef) newBitmapRGBA8ContextFromImage:(CGImageRef)image fromData:(uint8_t*)data {
    CGContextRef context = NULL;
    CGColorSpaceRef colorSpace;
    //uint32_t *bitmapData;
    
    const size_t bitsPerPixel = 32;
    const size_t bitsPerComponent = 8;
    const size_t bytesPerPixel = bitsPerPixel / bitsPerComponent;
    
    size_t width = CGImageGetWidth(image);
    size_t height = CGImageGetHeight(image);
    
    //NSLog(@"New: %zd %zd", width, height);
    
    size_t bytesPerRow = width * bytesPerPixel;
    //size_t bufferLength = bytesPerRow * height;
    
    colorSpace = CGColorSpaceCreateDeviceRGB();
    
    if(!colorSpace) {
        NSLog(@"Error allocating color space RGB\n");
        return nil;
    }
    
    //Create bitmap context
    context = CGBitmapContextCreate(data,
                                    width,
                                    height,
                                    bitsPerComponent,
                                    bytesPerRow,
                                    colorSpace,
                                    kCGImageAlphaPremultipliedLast);    // RGBA
    if(!context) {
        NSLog(@"Bitmap context not created");
    }
    
    CGColorSpaceRelease(colorSpace);
    
    return context;
}

-(void) convertUIImageToBitmapRGBA8:(UIImage*)image {
    CGImageRef imageRef = image.CGImage;
    
    // Create a bitmap context to draw the uiimage into
    CGContextRef context = [Controller newBitmapRGBA8ContextFromImage:imageRef fromData:data];
    
    if(!context) {
        return;
    }
    
    width = CGImageGetWidth(imageRef);
    height = CGImageGetHeight(imageRef);
    
    CGRect rect = CGRectMake(0, 0, width, height);
    
    //NSLog(@"[Debug] Ptr: %p", image);
    // Draw image into the context to get the raw image data
    CGContextDrawImage(context, rect, imageRef);
    
    CGContextRelease(context);
}

-(void)setWidth:(int)width_ andHeight:(int)height_ andTag:(int)tag_ andId:(int)id_ {
    //NSLog(@"In: %d, %d", width_, height_);
    tag = tag_;
    managerId = id_;
    frameRect = CGRectMake(0, 0, (float)width_ / scale, (float)height_ / scale);
    WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    webView = [[WKWebView alloc] initWithFrame:frameRect configuration:theConfiguration];
    
    webView.opaque = false;
    webView.clearsContextBeforeDrawing = true;
    webView.backgroundColor = UIColor.clearColor;
    webView.scrollView.backgroundColor = UIColor.clearColor;
}


-(void)loadURL:(NSString*) nsurl_str {
    webView.navigationDelegate = self;

    NSURL *nsurl=[NSURL URLWithString:nsurl_str];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [webView loadRequest:nsrequest];
}

-(void)setCallback:(IOS_Callback)callback_ andData:(uint8_t*)data_ andTag:(int)tag_ andId:(int)id_  {
    callback = callback_;
    data = data_;
    tag = tag_;
    managerId = id_;
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        IOS_printd("didFailNavigation: Navigation failed!");
        memset(data, 0, width * height * 4);
        struct IOS_CallbackBuffer callbackBuffer = { width, height, tag, data, managerId };
        callback(callbackBuffer);
    });
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        IOS_printd("didFailNavigation: Navigation failed!");
        memset(data, 0, width * height * 4);
        struct IOS_CallbackBuffer callbackBuffer = { width, height, tag, data, managerId };
        callback(callbackBuffer);
    });
}

- (void)webView:(WKWebView *)webView_ didFinishNavigation:(WKNavigation *)navigation
{
    
    if (self->callback != nil && self->data != nil) {
        memset(data, 0, width * height * 4);
        int tag_ = tag;
        [webView takeSnapshotWithConfiguration:nil completionHandler:^(UIImage * _Nullable snapshotImage, NSError * _Nullable error) {
            Controller *c = controllers[tag_];
            if (snapshotImage == nil) {
                IOS_printe("Obj-C: Null Browser image arrived!");
                struct IOS_CallbackBuffer callbackBuffer = {
                    c->width,
                    c->height,
                    c->tag,
                    c->data,
                    c->managerId };
                c->callback(callbackBuffer);
                return;
            }

            //NSLog(@"[Debug] Size: %zd %zd", c->width, c->height );
            [c convertUIImageToBitmapRGBA8:snapshotImage];
            
            struct IOS_CallbackBuffer callbackBuffer = {
                c->width, c->height, c->tag, c->data, c->managerId
            };
            
            /*
            NSLog(@"%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
                  (int)self->data[0], (int)self->data[1], (int)self->data[2], (int)self->data[3],
                  (int)self->data[4], (int)self->data[5], (int)self->data[6], (int)self->data[7],
                  (int)self->data[8], (int)self->data[9], (int)self->data[10], (int)self->data[11],
                  (int)self->data[12], (int)self->data[13], (int)self->data[14], (int)selbf->data[15]);
             */
            //NSLog(@"%zd", (uintptr_t)self->data);
            IOS_printd("didFinishNavigation: Calling back");
            c->callback(callbackBuffer);
            //callback(callbackBuffer);
        }];
    } else {
        IOS_printe("Obj-C: Browser image arrived, but no callback set!");
    }
    //});
}

void IOS_initWebView(int width, int height, int tag, int managerId) {
    //IOS_printd("IOS_initWebView: Dispatching async!");
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        if (controllers[tag] == nil) {
            IOS_printd("IOS_initWebView: Creating new controller!");
            controllers[tag] = [Controller new];
            [controllers[tag] setWidth:width andHeight:height andTag:tag andId:managerId];
        }
    });
}

void IOS_loadURL(const char* name, int tag) {
    //IOS_printd("IOS_loadURL: Dispatching asynch");
    //NSLog(@"Loading URL '%s'", name);
    NSString *nsurl_str = [NSString stringWithUTF8String:name];
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        //IOS_printd("IOS_loadURL: Loading URL!");
        [controllers[tag] loadURL:nsurl_str];
    });
}

void IOS_setCallback(IOS_Callback callback, uint8_t *data, int tag, int managerId) {
    //IOS_printd("IOS_setCallback: Dispatching async!");
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        //IOS_printd("IOS_setCallback: Setting callback!");
        [controllers[tag] setCallback:callback andData:data andTag:tag andId:managerId];
    });
}

void IOS_printv(const char* str) {
    NSLog(@"[Verbose] %s", str);
}

void IOS_printd(const char* str) {
    NSLog(@"[Debug] %s", str);
}

void IOS_printe(const char* str) {
    NSLog(@"[Error] %s", str);
}

void IOS_printw(const char* str) {
    NSLog(@"[Warning] %s", str);
}
@end

#else
// MAC
void IOS_initWebView(int width, int height, int tag, int managerId) {
    IOS_printd("MAC: IOS_initWebView Not implemented!");
}

void IOS_loadURL(const char* name, int tag) {}

void IOS_setCallback(IOS_Callback callback, uint8_t *data, int tag, int managerId) {}

void IOS_printv(const char* str) {
    NSLog(@"[Verbose] %s", str);
}

void IOS_printd(const char* str) {
    NSLog(@"[Debug] %s", str);
}

void IOS_printe(const char* str) {
    NSLog(@"[Error] %s", str);
}

void IOS_printw(const char* str) {
    NSLog(@"[Warning] %s", str);
}

#endif
#else
// ANDROID
void IOS_initWebView(int width, int height, int tag, int managerId) {}

void IOS_loadURL(const char* name, int tag) {}

void IOS_setCallback(IOS_Callback callback, uint8_t *data, int tag, int managerId) {}

void IOS_printv(const char* str) {}

void IOS_printd(const char* str) {}

void IOS_printe(const char* str) {}

void IOS_printw(const char* str) {}

#endif


