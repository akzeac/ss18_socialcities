#include "AndroidStuffPCH.h"
#include "AndroidManager.h"
#include "UELogger.h"
#include "Logic.h"

static const int Report_Period = 1000;
static const TimeVal Image_Delay_Max = 200;

static const int Default_Camera_Width = 1280;
static const int Default_Camera_Height = 720;
static const float Default_Camera_Exposure_Time = 0;
static const float Default_Camera_Exposure_Value = 0;
static const float Default_Camera_Intensity_Scale = 1;
static const float Default_Camera_Facing = (int)CameraFacing::Back; // back
static const int Default_Camera_API = (int)CameraAPI::V2;

using Markers::Mat24;

Logic *Logic::instance;
static int managerCounter = 0;

Logic* Logic::Create(AAndroidManager *manager) {
    if (instance == nullptr) {
        instance = new Logic();
    }
    
    instance->manager = manager;
    instance->managerId = ++managerCounter;
    Log::d("Creating Logic with counter ", instance->managerId);

    return instance;
}

void Logic::Release(AAndroidManager *manager) {
    Log::d("Releasing Logic with counter ", instance->managerId);
    std::lock_guard<std::mutex> lock(instance->managerMutex);
    if (manager == instance->manager) {
        instance->manager = nullptr;
        instance->managerId = -1;
    }
}

Logic::Logic() {
    if (Activity::IsIOS()) {
        Log::d("Creating IOS activity.");
        activity = std::make_unique<IOSActivity>(this);
    }else {
        Log::d("Creating Android activity.");
        activity =  std::make_unique<AndroidActivity>(this);
    }
	last_report = true;
	detach_next_image = false;
	next_cam_texture = nullptr;
    next_browser_texture = nullptr;
	use_half_image = true;
    managerId = -1;
}


Logic::~Logic() {
	executor.join();
	//testUnit.stop();
}

static const Color Board_Color = { 57, 103, 155, 255 };

void Logic::initialize() {
	camera_api = CameraAPI::V2;
	manager->CameraAPI = ECameraAPI::VE_API_2;


	manager->CameraWidth = Default_Camera_Width;
	manager->CameraHeight = Default_Camera_Height;
	manager->CameraExposureTime = Default_Camera_Exposure_Time;
	Log::d("Logic: Setting default camera size to ", manager->CameraWidth, 
		"x", manager->CameraHeight, " with exposure time ", manager->CameraExposureTime, ".");
	manager->CameraExposureValue = Default_Camera_Exposure_Value;

	manager->CameraIntensityScale = Default_Camera_Intensity_Scale;

	manager->FocalLengthX = C.I(0, 0);

	int facing_id = Default_Camera_Facing;
	manager->CameraFacing = (facing_id == (int)ECameraFacing::VE_Front) ?
		ECameraFacing::VE_Front : 
		ECameraFacing::VE_Back;


    available_sensors = activity->getAvailableSensors();
}

void Logic::onBeginPlay() {
	initialize();
	//testUnit.initialize(values);
	//testUnit.start();
}

void Logic::onTick() {
	TimeVal actualTime = Timer::getTicks();
	if (actualTime - last_report > Report_Period) {
        if (camera.size() != 0) {
            Log::v("Managed images: Camera ", camera.size());
        }
		last_report = actualTime;
	}
	manager->NumImagesCamera = camera.size();
}

CameraImage::Ptr Logic::getCameraImage(int width, int height, int y_pitch, int uv_pitch, CameraAPI camera_api_) {
	bool resized;
	CameraImage::Ptr image = camera.resize(width, height, y_pitch, uv_pitch, camera_api_, resized);
	if (resized) {
		manager->runInGameThread([this, width, height] {
			manager->CameraWidth = width;
			manager->CameraHeight = height;
			manager->RegenerateCameraTextures(width, height);
			manager->OnCameraSizeChanged();
		});
	}
	return image;
}


void Logic::onCameraImageArrived(const CameraImage::Ptr &buffer) {
/*
	executor.post([=]() {
		buffer->unpackY();
		manager->pushCameraY(buffer);
	});
	executor.post([=]() {
		buffer->unpackUV();
		manager->pushCameraUV(buffer);
	});
	if (detach_next_image) {
		UTexture2D *texture_to_write = next_cam_texture;
		next_cam_texture = nullptr;
		detach_next_image = false;

		executor.post([=] {
			Image::Ptr image; 
			if (use_half_image) {
				image = std::make_shared<Image>(buffer->width / 2, buffer->height / 2, 4);
				buffer->toColorImageHalf(*image);
			}
			else {
				image = std::make_shared<Image>(buffer->width, buffer->height, 4);
				buffer->toColorImage(*image);
			}
			manager->runInGameThread([=] {
				UTexture2D *texture_for_event;
				if (texture_to_write == nullptr ||
						texture_to_write->GetSurfaceWidth() != image->width ||
						texture_to_write->GetSurfaceHeight() != image->height) {
					// create new texture!
					texture_for_event = manager->writeToNewTexture(image);
				}
				else {
					// reuse old one
					manager->writeToTexture(texture_to_write, image);
					texture_for_event = texture_to_write;
				}
				manager->OnDetachedCameraTextureReady(texture_for_event);
			});
		});
	}
	manager->runInGameThread([this] {
		manager->OnCameraImageArrived();
	});
*/
}

void Logic::setDetachNextCameraImage(UTexture2D *texture) {
	detach_next_image = true;
	next_cam_texture = texture;
}

void Logic::onSensorDataArrived(const SensorData &sd) {
    std::lock_guard<std::mutex> lock(managerMutex);
    if (manager == nullptr) {
        Log::e("Logic: Rejecting sensor data. No manager exists.");
        return;
    }
	manager->runInGameThread([=] {
		switch (sd.type) {
		case SensorData::Type::Accelerometer:
			manager->Acceleration = { sd.data(0), sd.data(1), sd.data(2) };
			break;
		case SensorData::Type::Rotation_Vector:
			manager->Rotation = FQuat{ sd.data(0), sd.data(1), sd.data(2), sd.data(3) }.Rotator();
			break;
		case SensorData::Type::Linear_Acceleration:
			manager->LinearAcceleration = { sd.data(0), sd.data(1), sd.data(2) };
			break;
		case SensorData::Type::Game_Rotation_Vector:
		{
			const FQuat A_ = { 0, -1, 0, 0 };
			const FQuat C_ = { 0, -0.707107f, 0, 0.707107f };

			FQuat B_ = { sd.data(0), sd.data(1), sd.data(2), sd.data(3) };

			manager->GameRotation = B_.Rotator();
			manager->GameRotationUE = FRotator(A_ * B_ * C_);
		}
		break;
		}
	});
}

void Logic::onBrowserImageArrived(int index, int tag, int newManagerId) {
    std::lock_guard<std::mutex> lock(managerMutex);
    if (manager == nullptr) {
        Log::e("Logic: Rejecting browser image. No manager exists.");
        return;
    }
    if (newManagerId != managerId) {
        Log::e("Logic: Rejecting browser image. Manager id is incorrect. "
               "Expected ", managerId, ", got ", newManagerId, "!");
        return;
    }
    if (index != -1) {
        BrowserImage::Ptr buffer = browser.getImage(index);
        if (buffer == nullptr) {
            return; // shouldn't happen!
        }
        executor.post([=]() {
            buffer->flipRB();

            std::lock_guard<std::mutex> lock(managerMutex);
            if (manager == nullptr) {
                Log::e("Logic: Rejecting browser image. No manager exists.");
                return;
            }
            manager->runInGameThread([=] {
                manager->pushBrowserImage(index, buffer);
            });

        });
    } else {
        BrowserImage::Ptr image = browser.getSerialImage();
        if (image == nullptr) {
            return; // shouldn't happen!
        }
        UTexture2D *texture_to_write = next_browser_texture;
        next_browser_texture = nullptr;
        
        executor.post([=] {
            image->flipRB();
            
            std::lock_guard<std::mutex> lock(managerMutex);
            if (manager == nullptr) {
                Log::e("Logic: Rejecting browser image. No manager exists.");
                return;
            }
            manager->runInGameThread([=] {
                UTexture2D *texture_for_event;
                if (texture_to_write == nullptr ||
                    texture_to_write->GetSurfaceWidth() != image->width ||
                    texture_to_write->GetSurfaceHeight() != image->height) {
                    // create new texture!
                    Log::d("Logic: Creating new texture for serial browser.");
                    texture_for_event = manager->writeToBrowserTexture(nullptr, image);
                }
                else {
                    // reuse old one
                    Log::v("Logic: Reusing old texture for serial browser.");
                    texture_for_event = manager->writeToBrowserTexture(texture_to_write, image);
                }
                manager->OnSerialBrowserTextureReady(texture_for_event, tag);
            });
        });
    }
}

void Logic::onBrowserPropertyArrived(const std::string &property, const std::string &value) {
    if (manager == nullptr) {
        return;
    }
    manager->runInGameThread([=] {
        manager->OnBrowserPropertyArrived(FString(property.c_str()), FString(value.c_str()));
    });
}


void Logic::loadBrowserURL(int index, const std::string &url) {
	activity->setBrowserURL(index, url);
}

void Logic::renderBrowser(int index, int width, int height) {
	// already called from game thread
	bool resized;
	BrowserImage::Ptr image = browser.resize(index, width, height, resized);
	if (resized) {
		manager->RegenerateBrowserTexture(index, width, height);
	}
    std::fill(image->ptr, image->ptr + image->size, 0);
	activity->renderBrowser(index, width, height, image);
}

void Logic::loadBrowserURLAndRender(const std::string &url, int width, int height, UTexture2D *texture, int tag) {
    // already called from game thread
    BrowserImage::Ptr image = browser.resizeSerial(width, height);
    next_browser_texture = texture;
    activity->setBrowserURLAndRender(url, width, height, image, tag, managerId);
}

void Logic::loadBrowserURLAsControl(const std::string &url) {
    activity->setBrowserURLAsControl(url);
}

void Logic::detachBrowserImage(int index) {
	browser.detachImage(index);
}

void Logic::openCamera() {
	Log::d("Opening camera.");
	activity->openCamera();
}

void Logic::closeCamera() {
	Log::d("Closing camera.");
	activity->closeCamera();
}

void Logic::setCameraNearestSize(int width, int height) {
	if (width == 0 || height == 0) {
		width = manager->CameraWidth;
		height = manager->CameraHeight;
		Log::d("Logic: Using default camera size.");
	}
	else {
		Log::d("Logic: Overriding default camera size with ", width, "x", height, ".");
	}

	if (width == 0 || height == 0) {
		Log::e("Logic: Invalid camera size! Cannot initialize.");
	}
	else {
		int exposure_value = (camera_api == CameraAPI::V1) ? 
			manager->CameraExposureValue : manager->CameraExposureTime;
		activity->setCameraParameters(width, height, exposure_value);
	}
}

void Logic::startSensors() {
	Log::d("Starting sensors.");
	activity->startSensors();
	//testUnit.sensorsOn = true;
}

bool Logic::setupCamera(CameraAPI api, CameraFacing facing) {
	Log::d("Logic: Setting up camera facing ", 
		(facing == CameraFacing::Front) ? "front." : "back.");
	Log::d("Logic: Using camera API ", 
		(api == CameraAPI::V1) ? "1." : "2.");

	bool success = false;
	if (Activity::IsAndroid()) {
		success = activity->setupCamera(api, facing);
	}
	else {
		//testUnit.cameraOn = true;
		success = true;
	}
	if (success) {
		camera_api = api;
		manager->CameraAPI = (api == CameraAPI::V1) ?
			ECameraAPI::VE_API_1 :
			ECameraAPI::VE_API_2;
	}
	return success;
}

//const Values &Logic::getValues() const {
//	return values;
//}

static const float MetersToWorld = 100;
static const Vec3 Flip_Y_Left = Vec3(1, -1, 1);
static const Vec4 Flip_Y_Right = Vec4(1, -1, 1, MetersToWorld);

Vec3 Logic::TransformPointToUE(const Vec3 &p) {
	return Flip_Y_Left.asDiagonal() * p * MetersToWorld;
}

Aff3 Logic::TransformPoseToUE(const Aff3 &E) {
	Aff3 E_ue;
	E_ue.affine() = Flip_Y_Left.asDiagonal() * E.affine() * Flip_Y_Right.asDiagonal();
	return E_ue;
}

FTransform Logic::AffineToFTransform(const Aff3 &t) {
	FMatrix mat = {
		{ t(0, 0), t(1, 0), t(2, 0),  0 },
		{ t(0, 1), t(1, 1), t(2, 1),  0 },
		{ t(0, 2), t(1, 2), t(2, 2),  0 },
		{ t(0, 3), t(1, 3), t(2, 3),  1 }
	};
	return FTransform(mat);
}

void Logic::startIntent(const std::string &filename) {
	activity->startIntent(filename);
}

int Logic::getScreenOrientation() const {
	return activity->getScreenOrientation();
}

int Logic::getAvailableSensors() const {
    return available_sensors;
}
