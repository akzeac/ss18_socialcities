#include "AndroidStuffPCH.h"
#include "Browser.h"
#include "ImageUtils.h"

using Markers::ImageUtils;

BrowserImage::BrowserImage() : index(-1) { }

BrowserImage::BrowserImage(int width, int height, int index_) : 
		TimedImage(width, height, 4), index(index_)  { }

BrowserImage::Ptr Browser::resize(int index, int width, int height, bool &resized) {
	lock_guard lock(mutex);
	resized = false;
	if (!IsValidIndex(index)) {
		return nullptr;
	}
	BrowserImage::Ptr image = images[index];
	if (image == nullptr || image->width != width || image->height != height) {
		image = std::make_shared<BrowserImage>(width, height, index);
		images[index] = image;
		resized = true;
	}
	return image;
}

BrowserImage::Ptr Browser::getImage(int index) const {
	lock_guard lock(mutex);
	return IsValidIndex(index) ? images[index] : nullptr;
}

BrowserImage::Ptr Browser::resizeSerial(int width, int height) {
    lock_guard lock(mutex);
    BrowserImage::Ptr image = serialImage;
    if (image == nullptr || image->width != width || image->height != height) {
        image = std::make_shared<BrowserImage>(width, height, -1);
        serialImage = image;
    }
    return image;
}

BrowserImage::Ptr Browser::getSerialImage() const {
    lock_guard lock(mutex);
    return serialImage;
}

void Browser::detachImage(int index) {
	lock_guard lock(mutex);
	if (IsValidIndex(index)) {
		images[index] = nullptr;
	}
}

void BrowserImage::flipRB() {
	ImageUtils::Flip_RB(ptr, width, height);
}

