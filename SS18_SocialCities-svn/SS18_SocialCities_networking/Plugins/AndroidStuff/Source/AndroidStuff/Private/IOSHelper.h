//
//  IOSHelper.h
//  SocialCities
//
//  Created by Antonio Zea on 21.06.18.
//  Copyright © 2018 Epic Games, Inc. All rights reserved.
//

#ifndef IOSHelper_h
#define IOSHelper_h

#include <cstdlib>

extern "C" {
    struct IOS_CallbackBuffer {
        size_t width;
        size_t height;
        int tag;
        const uint8_t *data;
        int managerId;
    };

    typedef void (*IOS_Callback)(struct IOS_CallbackBuffer);

    void IOS_initWebView(int width, int height, int tag, int managerId);
    void IOS_loadURL(const char* name, int tag);
    void IOS_setCallback(IOS_Callback callback, uint8_t *data, int tag, int managerId);
    
    void IOS_printv(const char* str);
    void IOS_printd(const char* str);
    void IOS_printe(const char* str);
    void IOS_printw(const char* str);
};

#endif /* IOSHelper_h */
