// Some copyright should be here...

using UnrealBuildTool;
using System.IO;
using System;

public class AndroidStuff : ModuleRules
{
    private string WorkspacePath
    {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "..", "..", "..", "..", "..")); }
    }

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(WorkspacePath, "ThirdParty")); }
    }

    private string LibPath
    {
        get { return Path.GetFullPath(Path.Combine(WorkspacePath, "MC_Backend")); }
    }

    public AndroidStuff(ReadOnlyTargetRules Target) : base(Target)
    {
        //Definitions.Add("_DISABLE_EXTENDED_ALIGNED_STORAGE=1");
        PublicDependencyModuleNames.AddRange(new string[]
        { "Core", "CoreUObject", "Engine", "RHI", "RenderCore" });

        if (Target.Platform == UnrealTargetPlatform.Android)
        {
            string PluginPath = Utils.MakePathRelativeTo(ModuleDirectory, Target.RelativeEnginePath);
            PrivateDependencyModuleNames.AddRange(new string[] { "Launch" });
            AdditionalPropertiesForReceipt.Add(new ReceiptProperty("AndroidPlugin",
                Path.Combine(PluginPath, "Private/AndroidManager.xml")));
        }

        if (Target.Platform == UnrealTargetPlatform.Win64)
        {
            PublicDependencyModuleNames.AddRange(new string[] { "InputCore", "UMG" });
        }


        PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "AndroidStuff/Public"));
        PublicIncludePaths.Add(Path.Combine(LibPath, "Include"));

        //Console.WriteLine("path = " + MarkerLibPath);

        PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "Eigen"));

        if (Target.Platform == UnrealTargetPlatform.Win64)
        {
            PublicLibraryPaths.Add(Path.Combine(LibPath, "win64"));
            PublicAdditionalLibraries.Add("MC_Backend.lib");
        }
        else if (Target.Platform == UnrealTargetPlatform.Mac)
        {
            string BackendPath = Path.Combine(LibPath, "mac", "libMC_Backend.a");
            PublicAdditionalLibraries.Add(BackendPath);
        }
        else if (Target.Platform == UnrealTargetPlatform.IOS)
        {
            string BackendPath = Path.Combine(LibPath, "ios", "libMC_Backend.a");
            PublicAdditionalLibraries.Add(BackendPath);
            //PublicFrameworks.Add("/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS.sdk/System/Library/Frameworks/WebKit.framework");
            PublicFrameworks.AddRange(
            new string[] {
                "UIKit",
                "Foundation",
                "WebKit"
            }
            );
            
        }
        else if (Target.Platform == UnrealTargetPlatform.Android)
        {
            //string BackendPath = Path.Combine(LibPath, "android-64", "MC_Backend.a");
            //PublicAdditionalLibraries.Add(BackendPath);
            string BackendPath = Path.Combine(LibPath, "android", "MC_Backend.a");
            PublicAdditionalLibraries.Add(BackendPath);
        }
    }
}
