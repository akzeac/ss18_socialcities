#pragma once

#include "AndroidStuffPCH.h"
#include "DataReader.h"
#include "HandlerThread.h"

using Markers::SensorData;
using Markers::HandlerThread;
using Markers::Values;

class TestUnit {
private:
	HandlerThread thread;
	vector<SensorData> sds;
	vector<SensorData>::iterator sds_it;

	Timer timer;
	TimeVal start_time_real;
	TimeVal start_time_sensors;

	std::string basePath;
	Image::Ptr tmp_image;
	bool should_stop;

public:
	bool cameraOn;
	bool sensorsOn;

	TestUnit();
	void processImage(TimeVal timestamp);
	void processSensorData();
	bool initialize(const Values &values);
	void start();
	void stop();
	~TestUnit();
};
