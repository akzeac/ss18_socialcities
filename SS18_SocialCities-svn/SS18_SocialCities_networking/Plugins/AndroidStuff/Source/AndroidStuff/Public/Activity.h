#pragma once

#include "AndroidStuffPCH.h"
#include "CameraImage.h"
#include "Browser.h"


struct ActivityListener {
	virtual void onSensorDataArrived(const SensorData &sd) = 0;
	virtual CameraImage::Ptr getCameraImage(int width, int height, 
		int y_pitch, int uv_pitch, CameraAPI camera_api) = 0;
	virtual void onCameraImageArrived(const CameraImage::Ptr &buffer) = 0;
	virtual void onBrowserImageArrived(int index, int tag, int managerId) = 0;
    virtual void onBrowserPropertyArrived(const std::string &property, const std::string &value) = 0;
	virtual ~ActivityListener() {}
};

class Activity {
protected:
    enum class SensorType : int {
        TYPE_ACCELEROMETER = 1,
        TYPE_MAGNETIC_FIELD = 2,
        TYPE_LINEAR_ACCELERATION = 10,
        TYPE_GAME_ROTATION_VECTOR = 15
    };
    static ActivityListener *Instance;
    
public:
    static void OnSensorDataArrived(int type, const float *data, int size);
    static CameraImage::Ptr GetCameraImage(int width, int height,
                                           int y_pitch, int uv_pitch, CameraAPI camera_api);
    static void OnCameraImageArrived(const CameraImage::Ptr &buffer);
    static void OnBrowserImageArrived(int index, int tag, int managerId);
    static void OnBrowserPropertyArrived(const std::string &property, const std::string &value);
    
public:
    virtual bool setupCamera(CameraAPI api, CameraFacing facing) const = 0;
    
    virtual Vec2i setCameraParameters(int width, int height, float exposure_time_ms) = 0;
    
    virtual void openCamera() = 0;
    
    virtual void closeCamera() = 0;
    
    virtual uint8_t getActivityState() const = 0;

    static bool IsEditor() {
        return !IsAndroid() && !IsIOS();
    }

    static bool IsAndroid() {
#ifdef __ANDROID__
        return true;
#else
        return false;
#endif
    }

    static bool IsIOS() {
#if defined(__APPLE__)
        return true;
#else
        return false;
#endif
    }

    virtual void startSensors() = 0;
    
    virtual void setBrowserURL(int index, const std::string &url) = 0;
    
    virtual void renderBrowser(int index, int width, int height, const BrowserImage::Ptr &image) = 0;
    
    virtual void setBrowserURLAndRender(const std::string &url, int width, int height,
                                        const BrowserImage::Ptr &image, int tag, int managerId) = 0;
    
    virtual void setBrowserURLAsControl(const std::string &url) = 0;
    
    virtual void startIntent(const string &filename) = 0;
    
    virtual int getScreenOrientation() const = 0;
    
    virtual int getAvailableSensors() const = 0;
    
    virtual ~Activity() {}
};


class AndroidActivity : public Activity {
public:
	AndroidActivity(ActivityListener *Instance);

	~AndroidActivity();

	bool setupCamera(CameraAPI api, CameraFacing facing) const override;
	
	Vec2i setCameraParameters(int width, int height, float exposure_time_ms) override;
	
	void openCamera() override;
	
	void closeCamera() override;
	
	uint8_t getActivityState() const override;

	void startSensors() override;
    
	void setBrowserURL(int index, const std::string &url) override;

	void renderBrowser(int index, int width, int height, const BrowserImage::Ptr &image) override;
    
    void setBrowserURLAndRender(const std::string &url, int width, int height,
                                const BrowserImage::Ptr &image, int tag, int managerId) override;

    void setBrowserURLAsControl(const std::string &url) override;

    void startIntent(const string &filename) override;

	int getScreenOrientation() const override;

    int getAvailableSensors() const override;
};


class IOSActivity : public Activity {
public:
    IOSActivity(ActivityListener *Instance);
    
    ~IOSActivity();
    
    bool setupCamera(CameraAPI api, CameraFacing facing) const override;
    
    Vec2i setCameraParameters(int width, int height, float exposure_time_ms) override;
    
    void openCamera() override;
    
    void closeCamera() override;
    
    uint8_t getActivityState() const override;
    
    void startSensors() override;
    
    void setBrowserURL(int index, const std::string &url) override;
    
    void renderBrowser(int index, int width, int height, const BrowserImage::Ptr &image) override;
    
    void setBrowserURLAndRender(const std::string &url, int width, int height,
                                const BrowserImage::Ptr &image,
                                int tag, int managerId) override;
    
    void setBrowserURLAsControl(const std::string &url) override;
    
    void startIntent(const string &filename) override;
    
    int getScreenOrientation() const override;
    
    int getAvailableSensors() const override;
};
