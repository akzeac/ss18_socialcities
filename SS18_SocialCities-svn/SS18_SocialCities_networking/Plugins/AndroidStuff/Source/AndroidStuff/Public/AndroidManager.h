// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AndroidStuffPCH.h"
#include <EngineMinimal.h>
#include "UEPlotter.h"
#include "Browser.h"
#include "CameraImage.h"
#include "Logic.h"
#include "AndroidManager.generated.h"

using Markers::DelayedQueue;

UENUM(BlueprintType) enum class ECameraFacing : uint8 {
	VE_Front = 0	UMETA(DisplayName = "Front"),
	VE_Back = 1		UMETA(DisplayName = "Back"),
};

UENUM(BlueprintType) enum class ECameraAPI : uint8 {
    VE_API_0 = 0    UMETA(DisplayName = "Unset"),
	VE_API_1 = 1	UMETA(DisplayName = "API 1"),
	VE_API_2 = 2	UMETA(DisplayName = "API 2"),
};

UENUM(BlueprintType) enum class EAndroidOrientation : uint8 {
	VE_ROTATION_0 = 0	UMETA(DisplayName = "Rotation 0"),
	VE_ROTATION_90 = 1	UMETA(DisplayName = "Rotation 90"),
	VE_ROTATION_180 = 2	UMETA(DisplayName = "Rotation 180"),
	VE_ROTATION_270 = 3	UMETA(DisplayName = "Rotation 270"),
};

UENUM(BlueprintType) enum class EAndroidSensorType : uint8 {
    VE_TYPE_ACCELEROMETER = 1           UMETA(DisplayName = "Accelerometer"),
    VE_TYPE_LINEAR_ACCELERATION = 2     UMETA(DisplayName = "Linear Acceleration"),
    VE_TYPE_GAME_ROTATION_VECTOR = 4    UMETA(DisplayName = "Game Rotation Vector"),
    VE_TYPE_GYROSCOPE = 8               UMETA(DisplayName = "Gyroscope"),
};



UCLASS()
class ANDROIDSTUFF_API AAndroidManager : public AActor {
	GENERATED_BODY()

private:
	Logic *logic;

	DelayedQueue<CameraImage> y_queue;
	DelayedQueue<CameraImage> uv_queue;

	bool must_stop;

	std::mutex runMutex;
	list<Functor> runQueue;
	void runGameThreadQueue();

	void EnqueueTextureY();
	void EnqueueTextureUV();
	void EnqueueTexture(UTexture2D *texture, const Image::Ptr &image) const;
	void runInRenderThread(const Functor &f) const;
	
	static FString GetDefaultSavedDir();
	static FString GetDefaultSavedDirAbsolute();

private:
    static AAndroidManager *instance;

public:
	AAndroidManager();
	virtual ~AAndroidManager();

	void RegenerateCameraTextures(int width, int height);
	void RegenerateBrowserTexture(int index, int width, int height);
	void runInGameThread(const Functor &f);
	void pushCameraY(const CameraImage::Ptr &img);
	void pushCameraUV(const CameraImage::Ptr &img);
	void pushBrowserImage(int index, const BrowserImage::Ptr &img);
	UTexture2D *writeToNewTexture(const Image::Ptr &image) const;
	void writeToTexture(UTexture2D *texture, const Image::Ptr &image) const;
    
    UTexture2D *writeToBrowserTexture(UTexture2D *texture, const Image::Ptr &image);

    
public:
	UPROPERTY(Transient, BlueprintReadOnly) UTexture2D *CameraTextureY;
	UPROPERTY(Transient, BlueprintReadOnly) UTexture2D *CameraTextureU;
	UPROPERTY(Transient, BlueprintReadOnly) UTexture2D *CameraTextureV;
	UPROPERTY(Transient, BlueprintReadOnly) TArray<UTexture2D*> BrowserTextures;
	
	UPROPERTY(BlueprintReadOnly) float FocalLengthX;
	UPROPERTY(BlueprintReadOnly) FVector Acceleration;
	UPROPERTY(BlueprintReadOnly) FVector MagneticField;
	UPROPERTY(BlueprintReadOnly) FVector LinearAcceleration;
	UPROPERTY(BlueprintReadOnly) FRotator Rotation;
	UPROPERTY(BlueprintReadOnly) FRotator GameRotation;
	UPROPERTY(BlueprintReadWrite) FRotator GameRotationUE;
	UPROPERTY(BlueprintReadOnly) FTransform LastEstimatedPose;

	//UPROPERTY(BlueprintReadWrite) UUserWidget *BrowserWidget;

	UPROPERTY(BlueprintReadOnly) int CameraWidth;
	UPROPERTY(BlueprintReadOnly) int CameraHeight;
	UPROPERTY(BlueprintReadOnly) float CameraExposureTime;
	UPROPERTY(BlueprintReadOnly) float CameraExposureValue;
	UPROPERTY(BlueprintReadOnly) int CameraDelay;

	UPROPERTY(BlueprintReadWrite) float CameraIntensityScale;

	UPROPERTY(BlueprintReadOnly) int DelaysCamera;
	UPROPERTY(BlueprintReadOnly) int FPSCamera;

	UPROPERTY(BlueprintReadOnly) int NumImagesCamera;

	UPROPERTY(BlueprintReadOnly) ECameraAPI CameraAPI;
	UPROPERTY(BlueprintReadOnly) ECameraFacing CameraFacing;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

    UFUNCTION(BlueprintPure, Category = "Android|Base")
    static AAndroidManager* GetAndroidManager();
    
	UFUNCTION(BlueprintCallable, Category = "Android|Camera") 
	void SetupCamera(ECameraAPI api, ECameraFacing facing, bool &success);

	UFUNCTION(BlueprintCallable, Category = "Android|Camera")
	void SetCameraNearestSize(int width, int height);

	UFUNCTION(BlueprintCallable, Category = "Android|Camera")
	void OpenCamera();

	UFUNCTION(BlueprintCallable, Category = "Android|Camera")
	void CloseCamera();

	UFUNCTION(BlueprintCallable, Category = "Android|Sensors")
	void StartSensors();

	UFUNCTION(BlueprintCallable, Category = "Android|Sensors")
	EAndroidOrientation GetScreenOrientation() const;
    
    UFUNCTION(BlueprintCallable, Category = "Android|Sensors")
    bool IsSensorAvailable(EAndroidSensorType t) const;
	
	UFUNCTION(BlueprintCallable, Category = "Android|Maths")
	static void PrintTransform(FTransform A);

    /*
	UFUNCTION(BlueprintPure, Category = "Android|Values")
	int GetIntegerValue(FString key, int Default) const;

	UFUNCTION(BlueprintPure, Category = "Android|Values")
	float GetFloatValue(FString key, float Default) const;

	UFUNCTION(BlueprintPure, Category = "Android|Values")
	FString GetStringValue(FString key, FString Default) const;

	UFUNCTION(BlueprintPure, Category = "Android|Values")
	bool GetBooleanValue(FString key, bool Default) const;
    */

	UFUNCTION(BlueprintPure, Category = "Android|Base")
	static bool IsAndroid() {
#ifdef __ANDROID__
		return true;
#else
		return false;
#endif
	}
    
    UFUNCTION(BlueprintPure, Category = "Android|Base")
    static bool IsIOS() {
#if defined(__APPLE__)
        return true;
#else
        return false;
#endif
    }

	UFUNCTION(BlueprintCallable, Category = "Android|Browsers")
	void LoadBrowserURL(int index, FString url);

    UFUNCTION(BlueprintCallable, Category = "Android|Browsers")
    void LoadBrowserURLAsControl(FString url);

	UFUNCTION(BlueprintCallable, Category = "Android|Browsers")
	void RenderBrowser(int index,int width, int height);

	UFUNCTION(BlueprintCallable, Category = "Android|Browsers")
	void DetachBrowserTexture(int index);

    UFUNCTION(BlueprintCallable, Category = "Android|Browsers")
    void LoadBrowserURLAndRender(FString url, int width, int height, UTexture2D *texture = nullptr, int tag = 0);
    
    UFUNCTION(BlueprintCallable, Category = "Android|Camera")
	void DetachCameraTexture(UTexture2D *texture = nullptr);

	UFUNCTION(BlueprintCallable, Category = "Android|Camera")
	static void DeleteTransientTexture(UTexture2D *texture);

	UFUNCTION(BlueprintPure, Category = "Android|Browsers")
	static int GetNumberOfBrowsers() {
		return Browser::Size;
	}

    UFUNCTION(BlueprintCallable, Category = "Android|Browsers")
    UTexture2D *CreateTransparentTexture() const;
    
	UFUNCTION(BlueprintCallable, Category = "Android|Screenshot")
	void CreateScreenshot(FString fileName);

	UFUNCTION(BlueprintCallable, Category = "Android|Screenshot")
	void StartShareImageIntent(FString fileName);

	UFUNCTION(BlueprintImplementableEvent, Category = "Android|Base")
	void OnInitialize();

	UFUNCTION(BlueprintImplementableEvent, Category = "Android|Camera")
	void OnCameraSizeChanged();

	UFUNCTION(BlueprintImplementableEvent, Category = "Android|Camera")
	void OnCameraImageArrived();

	UFUNCTION(BlueprintImplementableEvent, Category = "Android|Camera")
	void OnCameraTexturesChanged();

	UFUNCTION(BlueprintImplementableEvent, Category = "Android|Browser")
	void OnBrowserTextureReady(int index, UTexture2D *texture);

	UFUNCTION(BlueprintImplementableEvent, Category = "Android|Browser")
	void OnBrowserImageReady(int index);

    UFUNCTION(BlueprintImplementableEvent, Category = "Android|Browser")
    void OnBrowserPropertyArrived(const FString &property, const FString &value);

    UFUNCTION(BlueprintImplementableEvent, Category = "Android|Browser")
    void OnSerialBrowserTextureReady(UTexture2D *texture, int tag);
    
	UFUNCTION(BlueprintImplementableEvent, Category = "Android|Camera")
	void OnDetachedCameraTextureReady(UTexture2D *texture);
};


