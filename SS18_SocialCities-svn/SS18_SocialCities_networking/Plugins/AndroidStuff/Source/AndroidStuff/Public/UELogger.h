#pragma once

#include "AndroidStuffPCH.h"

struct UEScreenLog : public Markers::Logger {
	void s(const std::string &msg, const Color &color);
    
    virtual ~UEScreenLog() {}
};

struct UEConsoleLog : public UEScreenLog {
	static const char *Tag;

	void d(const std::string &msg);

	void e(const std::string &msg);

	void w(const std::string &msg);

	void v(const std::string &msg);
    
    virtual ~UEConsoleLog() {}
};

struct AndroidLog : public UEScreenLog {
	static const char *Tag;

	void d(const std::string &msg);

	void e(const std::string &msg);

	void w(const std::string &msg);

	void v(const std::string &msg);
};

struct IOSLog : public UEScreenLog {
    static const char *Tag;
    
    void d(const std::string &msg);
    
    void e(const std::string &msg);
    
    void w(const std::string &msg);
    
    void v(const std::string &msg);
};




