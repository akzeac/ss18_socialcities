\contentsline {section}{\numberline {1}Einleitung}{7}{section.5}
\contentsline {subsection}{\numberline {1.1}Aufgabenstellung}{7}{subsection.10}
\contentsline {section}{\numberline {2}ARCore}{8}{section.12}
\contentsline {subsection}{\numberline {2.1}Ben\IeC {\"o}tigte Features}{8}{subsection.14}
\contentsline {subsection}{\numberline {2.2}Funktionsweise}{8}{subsection.15}
\contentsline {subsubsection}{\numberline {2.2.1}Motion Tracking}{8}{subsubsection.16}
\contentsline {subsubsection}{\numberline {2.2.2}User Interaction}{9}{subsubsection.18}
\contentsline {section}{\numberline {3}Konzept und Terminologie des Stadtaufbaus}{10}{section.19}
\contentsline {subsection}{\numberline {3.1}Terminologie}{10}{subsection.20}
\contentsline {paragraph}{Room}{10}{section*.22}
\contentsline {paragraph}{Chunk}{10}{section*.23}
\contentsline {paragraph}{ChunkBuilding}{11}{section*.25}
\contentsline {paragraph}{Building}{11}{section*.27}
\contentsline {paragraph}{BuildingBlock}{11}{section*.28}
\contentsline {paragraph}{Kalibierungspunkt}{12}{section*.29}
\contentsline {subsection}{\numberline {3.2}Stadtaufbau}{12}{subsection.30}
\contentsline {subsection}{\numberline {3.3}Interaktionsm\IeC {\"o}glichkeiten mit der Stadt}{12}{subsection.31}
\contentsline {subsection}{\numberline {3.4}Materials der BuildingBlocks}{12}{subsection.33}
\contentsline {subsection}{\numberline {3.5}Screenflow}{12}{subsection.34}
\contentsline {section}{\numberline {4}Multiuser-Experience}{13}{section.35}
\contentsline {subsection}{\numberline {4.1}Realisierungsm\IeC {\"o}glichkeit 1: Cloud Anchors}{13}{subsection.36}
\contentsline {subsection}{\numberline {4.2}Realisierungsm\IeC {\"o}glichkeit 2: Unreal Networking Feature}{13}{subsection.37}
\contentsline {subsubsection}{\numberline {4.2.1}Actor Replication}{13}{subsubsection.38}
\contentsline {paragraph}{Property Updates}{14}{section*.39}
\contentsline {paragraph}{Remote Procedure Calls}{14}{section*.40}
\contentsline {subsubsection}{\numberline {4.2.2}Gameplay-Klassen}{14}{subsubsection.41}
\contentsline {paragraph}{GameInstance}{14}{section*.42}
\contentsline {paragraph}{GameMode}{14}{section*.43}
\contentsline {paragraph}{GameState}{14}{section*.44}
\contentsline {paragraph}{PlayerController}{15}{section*.45}
\contentsline {paragraph}{PlayerState}{15}{section*.46}
\contentsline {paragraph}{Pawn}{15}{section*.47}
\contentsline {subsubsection}{\numberline {4.2.3}Arten von Servern}{15}{subsubsection.49}
\contentsline {paragraph}{Dedicated Server}{15}{section*.50}
\contentsline {paragraph}{Listen Server}{15}{section*.51}
\contentsline {subsection}{\numberline {4.3}Umsetzung mit Unreal Networking Feature}{16}{subsection.52}
\contentsline {section}{\numberline {5}Evaluierung}{17}{section.53}
\contentsline {section}{\numberline {6}Setup der Entwicklungsumgebung}{18}{section.54}
\contentsline {subsection}{\numberline {6.1}Unreal Engine mit ARCore}{18}{subsection.55}
\contentsline {subsection}{\numberline {6.2}Deployment der App}{18}{subsection.56}
