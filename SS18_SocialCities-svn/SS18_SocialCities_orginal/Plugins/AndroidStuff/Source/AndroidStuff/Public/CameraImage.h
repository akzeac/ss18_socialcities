#pragma once

#include "AndroidStuffPCH.h"

enum class CameraAPI {
	Invalid = 0, V1 = 1, V2 = 2
};

enum class CameraFacing {
	Front = 0, Back = 1
};

class CameraImage {
public:
	using Ptr = std::shared_ptr<CameraImage>;
	using ConstPtr = std::shared_ptr<const CameraImage>;
	using WeakPtr = std::weak_ptr<const CameraImage>;

private:
	vector<uint8_t> raw_yuv;

public:
	Image y;
	Image u;
	Image v;
	const int width;
	const int height;
	const int y_pitch;
	const int uv_pitch;
	const CameraAPI camera_api;
	uint8_t *const raw_ptr;
	const size_t raw_size;
	TimeVal timestamp;

public:
	CameraImage();

	CameraImage(int width, int height, int y_pitch, int uv_pitch, CameraAPI camera_api);

	void unpackY();

	void unpackUV();

	void toColorImage(Image &rgba) const;

	void toColorImageHalf(Image &rgba) const;
};
