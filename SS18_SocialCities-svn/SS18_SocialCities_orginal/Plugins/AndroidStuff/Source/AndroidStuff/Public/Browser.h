#pragma once

#include "AndroidStuffPCH.h"

class BrowserImage : public TimedImage {
public:
	using Ptr = std::shared_ptr<BrowserImage>;
	using ConstPtr = std::shared_ptr<const BrowserImage>;
	using WeakPtr = std::weak_ptr<const BrowserImage>;
	const int index;

	BrowserImage();

	BrowserImage(int width, int height, int index);

	void flipRB();
};

class Browser {
public:
	static const int Size = 2;

private:
	mutable std::mutex mutex;
	array<BrowserImage::Ptr, Size> images;
   // Markers::Repository<CameraImage>
    BrowserImage::Ptr serialImage;

public:
	BrowserImage::Ptr resize(int index, int width, int height, bool &resized);

	BrowserImage::Ptr getImage(int index) const;
    
    BrowserImage::Ptr resizeSerial(int width, int height);

    BrowserImage::Ptr getSerialImage() const;

	void detachImage(int index);

	static bool IsValidIndex(int i) {
		return i >= 0 && i < Size;
	}
};

