#pragma once

#include "AndroidStuffPCH.h"
#include "Activity.h"
#include "TestUnit.h"
#include "Camera.h"

using Markers::Executor;
using Markers::CameraInfo;
using std::unique_ptr;
class AAndroidManager;
class UTexture2D;

class Logic : public ActivityListener {
private:
	Executor executor;
	unique_ptr<Activity> activity;
	//TestUnit testUnit;
	Camera camera;
	Browser browser;
	AAndroidManager *manager;
	CameraInfo C;
	TimeVal last_report;
	CameraAPI camera_api;
    
    uint32_t available_sensors;

	bool use_half_image;
	bool detach_next_image;
    UTexture2D *next_cam_texture;
    UTexture2D *next_browser_texture;

	void initialize();

public:
	Logic(AAndroidManager *manager);
	~Logic();
	void onTick();
	void onBeginPlay();

	void onSensorDataArrived(const SensorData &sd) override;
	CameraImage::Ptr getCameraImage(int width, int height, 
		int y_pitch, int uv_pitch, CameraAPI camera_api) override;
	void onCameraImageArrived(const CameraImage::Ptr &buffer) override;
	void onBrowserImageArrived(int index, int tag) override;
    void onBrowserPropertyArrived(const std::string &property, const std::string &value);

	bool setupCamera(CameraAPI api, CameraFacing facing);
	void openCamera();
	void closeCamera();
	void setCameraNearestSize(int width, int height);
	void startSensors();
	
	void loadBrowserURL(int index, const std::string &url);
    void loadBrowserURLAsControl(const std::string &url);
	void renderBrowser(int index, int width, int height);
	void detachBrowserImage(int index);
    void loadBrowserURLAndRender(const std::string &url, int width, int height, UTexture2D *texture, int tag);

	void setDetachNextCameraImage(UTexture2D *texture);

	//const Values &getValues() const;

	void startIntent(const std::string &filename);

	int getScreenOrientation() const;
    
    int getAvailableSensors() const;

public:
	static Vec3 TransformPointToUE(const Vec3 &p);
	static Aff3 TransformPoseToUE(const Aff3 &E);
	static FTransform AffineToFTransform(const Aff3 &E);
};
