#pragma once

#include "Plotter.h"
#include "AndroidStuffPCH.h"

class UWorld;

class UEPlotter : public Markers::Plotter {
private:
	std::mutex mutex;

	void plotGlobal(UWorld *world, float thick);

	void plotLocal(UWorld *world, const Aff2 &Iinv, const Aff3 &E, float z, float thick);
	
public:
	void append(const UEPlotter &plotter);

	void plotToUE(UWorld *world, const Aff2 &Iinv, const Aff3 &E, float z = 3.f, float thick = 0.5f);
};