#include "AndroidStuffPCH.h"
#include "AndroidManager.h"
#include "DrawDebugHelpers.h"
#include "Logic.h"
#include "UELogger.h"

#include "Runtime/Engine/Classes/DeviceProfiles/DeviceProfileManager.h"

static string to_string(const FString &str) {
	return string(TCHAR_TO_UTF8(*str));
}

static FString from_string(const string &str) {
	return FString(str.c_str());
}

static AAndroidManager *instance;

// Sets default values
AAndroidManager::AAndroidManager() {
	PrimaryActorTick.bCanEverTick = true;
	CameraTextureY = nullptr;
	CameraTextureU = nullptr;
	CameraTextureV = nullptr;
	CameraDelay = 0;
	BrowserTextures.SetNum(Browser::Size);
	for (int i = 0; i < Browser::Size; i++) {
		BrowserTextures[i] = nullptr;
	}
	//BrowserWidget = nullptr;
	Timer::setBase();
	if (IsAndroid()) {
		Log::Logger = std::make_unique<AndroidLog>();
	}
    else if (IsIOS()) {
        Log::Logger = std::make_unique<IOSLog>();
    }
	else {
		Log::Logger = std::make_unique<UEConsoleLog>();
	}
 	must_stop = false;
    instance = this;
}

AAndroidManager::~AAndroidManager() {
	must_stop = true;
	logic = nullptr;
}

// Called when the game starts or when spawned
void AAndroidManager::BeginPlay() {
	Super::BeginPlay();

	Log::d("Manager: Creating logic.");
	logic = std::make_unique<Logic>(this);
	logic->onBeginPlay();

	FString profile_name = UDeviceProfileManager::GetActiveProfileName();
	Log::d("Profile: ", to_string(profile_name));

	OnInitialize();
}

// Called every frame
void AAndroidManager::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	if (CameraTextureY != nullptr) {
		runInRenderThread([this]() {
			EnqueueTextureY();
		});
	}
	if (CameraTextureU != nullptr && CameraTextureV != nullptr) {
		runInRenderThread([this]() {
			EnqueueTextureUV();
		});
	}
	runGameThreadQueue();
	logic->onTick();
}

void AAndroidManager::runInRenderThread(const Functor &fun) const {
	ENQUEUE_UNIQUE_RENDER_COMMAND_ONEPARAMETER(
		UpdateTextureRegionsData,
		std::function<void()>, fun, fun,
		{ fun(); }
	);
}

void AAndroidManager::runInGameThread(const Functor &f) {
	std::lock_guard<std::mutex> lock(runMutex);
	runQueue.push_back(f);
}

void AAndroidManager::runGameThreadQueue() {
	list<Functor> runQueue_;
	{
		lock_guard lock(runMutex);
		runQueue_.swap(runQueue);
	}
	for (Functor &fun : runQueue_) {
		fun();
	}
}

static void _rt_writeTexture(UTexture2D* texture, uint32_t width, uint32_t height, 
		int bpp, int pitch, const uint8* data) {
	// this function may only be called from the render thread!
	if (texture == nullptr || texture->Resource == nullptr) {
		Log::w("writeTexture: Rejecting null texture!");
		return;
	}
	if (width != texture->PlatformData->SizeX || height != texture->PlatformData->SizeY) {
		Log::w("writeTexture: Rejecting texture of unexpected size!");
		return;
	}
	if (data == nullptr) {
		Log::w("writeTexture: Rejecting null source!");
		return;
	}
	FTexture2DResource *resource = (FTexture2DResource*)texture->Resource;
	int32 currentFirstMip = resource->GetCurrentFirstMip();
	FUpdateTextureRegion2D region = { 0, 0, 0, 0, width, height };
	if (currentFirstMip == 0) {
		RHIUpdateTexture2D(resource->GetTexture2DRHI(), 0, region, pitch, data);
	}
}

void AAndroidManager::EnqueueTextureY() {
	if (must_stop) {
		return;
	}
	TimeVal time = Timer::getTicks();
	CameraImage::ConstPtr y_buffer = y_queue.pop(time - CameraDelay);
	if (y_buffer != nullptr) {
		const Image &y = y_buffer->y;
		_rt_writeTexture(CameraTextureY, y.width, y.height, 1, y.pitch, y.ptr);
	}
}

void AAndroidManager::EnqueueTextureUV() {
	if (must_stop) {
		return;
	}
	TimeVal time = Timer::getTicks();
	CameraImage::ConstPtr uv_buffer = uv_queue.pop(time - CameraDelay);
	if (uv_buffer != nullptr) {
		const Image &u = uv_buffer->u;
		const Image &v = uv_buffer->v;
		_rt_writeTexture(CameraTextureU, u.width, u.height, 1, u.pitch, u.ptr);
		_rt_writeTexture(CameraTextureV, v.width, v.height, 1, v.pitch, v.ptr);
	}
}

void AAndroidManager::EnqueueTexture(UTexture2D *texture, const Image::Ptr &image)  const {
	if (must_stop) {
		return;
	}
	_rt_writeTexture(texture, image->width, image->height, 4, image->pitch, image->ptr);
}


static UTexture2D* CreateTransientDynamicTexture(int width, int height, EPixelFormat format) {
	Log::d("Generating transient texture of size ", width, "x", height, ".");
	UTexture2D* Texture = UTexture2D::CreateTransient(width, height, format);
	Texture->Filter = TextureFilter::TF_Nearest;
	Texture->SRGB = 0;
	Texture->UpdateResource();
	return Texture;
}

void AAndroidManager::RegenerateCameraTextures(int width, int height) {
	Log::d("Regenerating camera textures.");
	CameraTextureY = CreateTransientDynamicTexture(width, height, PF_A8);
	CameraTextureU = CreateTransientDynamicTexture(width / 2, height / 2, PF_A8);
	CameraTextureV = CreateTransientDynamicTexture(width / 2, height / 2, PF_A8);
	CameraTextureY->Filter = TextureFilter::TF_Default;
	CameraTextureU->Filter = TextureFilter::TF_Default;
	CameraTextureV->Filter = TextureFilter::TF_Default;

	// already in game thread
	OnCameraTexturesChanged();
}

void AAndroidManager::RegenerateBrowserTexture(int index, int width, int height) {
	Log::d("Regenerating browser texture ", index, ".");
	if (!Browser::IsValidIndex(index)) {
		Log::e("Invalid browser texture index!");
		return;
	}
	BrowserTextures[index] = CreateTransientDynamicTexture(width, height, PF_B8G8R8A8);
	BrowserTextures[index]->Filter = TextureFilter::TF_Bilinear;
	OnBrowserTextureReady(index, BrowserTextures[index]);
}

void AAndroidManager::pushCameraY(const CameraImage::Ptr &img) {
	y_queue.push(img);
}

void AAndroidManager::pushCameraUV(const CameraImage::Ptr &img) {
	uv_queue.push(img);
}

void AAndroidManager::pushBrowserImage(int index, const BrowserImage::Ptr &img) {
	UTexture2D *texture = BrowserTextures[index];
	runInRenderThread([=]() {
		EnqueueTexture(texture, img);
	});
	OnBrowserImageReady(index);
}

UTexture2D *AAndroidManager::writeToNewTexture(const Image::Ptr &img) const {
	UTexture2D *texture = CreateTransientDynamicTexture(img->width, img->height, PF_B8G8R8A8);
	writeToTexture(texture, img);
	return texture;
}

void AAndroidManager::writeToTexture(UTexture2D *texture, const Image::Ptr &img) const {
	runInRenderThread([=]() {
		EnqueueTexture(texture, img);
	});
}

UTexture2D *AAndroidManager::writeToBrowserTexture(UTexture2D *texture, const Image::Ptr &img) {
    if (texture == nullptr) {
        texture = CreateTransientDynamicTexture(img->width, img->height, PF_B8G8R8A8);
    }
    runInRenderThread([=]() {
        EnqueueTexture(texture, img);
        //runInGameThread([this, texture] {
        //});
    });
    //OnSerialBrowserTextureReady(texture);
    return texture;
}

UTexture2D *AAndroidManager::CreateTransparentTexture() const {
    Image::Ptr img = std::make_shared<Image>(64, 64, 4);
    std::fill(img->ptr, img->ptr + img->size, 0);
    return writeToNewTexture(img);
}


void AAndroidManager::SetupCamera(ECameraAPI api, ECameraFacing facing, bool &ret) {
	ret = logic->setupCamera(
		(api == ECameraAPI::VE_API_1) ?
		CameraAPI::V1 :
		CameraAPI::V2,
		(facing == ECameraFacing::VE_Front) ?
		CameraFacing::Front : 
		CameraFacing::Back
	);
	if (ret) {
		CameraAPI = api;
		CameraFacing = facing;
	}
}

void AAndroidManager::OpenCamera() {
	logic->openCamera();
}

void AAndroidManager::CloseCamera() {
	logic->closeCamera();
}

void AAndroidManager::SetCameraNearestSize(int width, int height) {
	logic->setCameraNearestSize(width, height);
}

void AAndroidManager::StartSensors() {
	logic->startSensors();
}

void AAndroidManager::PrintTransform(FTransform A) {
	FMatrix M = A.ToMatrixWithScale();
	std::ostringstream o;
	o << "[Transform\n" <<
		"[" << M.M[0][0] << ", " << M.M[1][0] << ", " << M.M[2][0] <<", " << M.M[3][0] <<";\n" <<
		" " << M.M[0][1] << ", " << M.M[1][1] << ", " << M.M[2][1] <<", " << M.M[3][1] <<";\n" <<
		" " << M.M[0][2] << ", " << M.M[1][2] << ", " << M.M[2][2] <<", " << M.M[3][2] <<";\n" <<
		" " << M.M[0][3] << ", " << M.M[1][3] << ", " << M.M[2][3] <<", " << M.M[3][3] <<"] ]";
	Log::d(o.str());
}

/*
int AAndroidManager::GetIntegerValue(FString key, int def) const {
	return logic->getValues().getInteger(to_string(key), def);
}

float AAndroidManager::GetFloatValue(FString key, float def) const {
	return logic->getValues().getFloat(to_string(key), def);
}

FString AAndroidManager::GetStringValue(FString key, FString def) const {
	return from_string(logic->getValues().getString(to_string(key), to_string(def)));
}

bool AAndroidManager::GetBooleanValue(FString key, bool def) const {
	return logic->getValues().getBoolean(to_string(key), def);
}
*/

void AAndroidManager::LoadBrowserURL(int index, FString url) {
	if (Browser::IsValidIndex(index)) {
		std::string url_str = to_string(url);
		Log::v("Loading browser index ", index, " with url '", url_str, "'");
		logic->loadBrowserURL(index, url_str);
	}
}

void AAndroidManager::LoadBrowserURLAsControl(FString url) {
    std::string url_str = to_string(url);
    Log::v("Loading control browser url '", url_str, "'");
    logic->loadBrowserURLAsControl(url_str);
}

void AAndroidManager::RenderBrowser(int index, int width, int height) {
	if (width <= 0 || height <= 0) {
		Log::e("Invalid render browser size ", width, "x", height);
		return;
	}
	if (Browser::IsValidIndex(index)) {
		Log::v("Rendering browser index ", index);
		logic->renderBrowser(index, width, height);
	}
}

void AAndroidManager::LoadBrowserURLAndRender(FString url, int width, int height, UTexture2D *texture, int tag) {
    if (width <= 0 || height <= 0) {
        Log::e("Invalid render browser size ", width, "x", height);
        return;
    }
    std::string url_str = to_string(url);
    Log::v("Loading and rendering serial browser with url '", url_str, "'");
    logic->loadBrowserURLAndRender(url_str, width, height, texture, tag);
}

void AAndroidManager::DetachBrowserTexture(int index) {
	if (Browser::IsValidIndex(index)) {
		Log::d("Detaching browser texture ", index, ".");
		BrowserTextures[index] = nullptr;
		logic->detachBrowserImage(index);
	}
}

void AAndroidManager::DetachCameraTexture(UTexture2D *texture) {
	Log::d("Detaching camera texture.");
	logic->setDetachNextCameraImage(texture);
}

void AAndroidManager::StartShareImageIntent(FString fileName) {
	string fullpath_str = to_string(GetDefaultSavedDirAbsolute() + fileName);
	Log::d("Starting Share Intent with image '", fullpath_str, "'");
	logic->startIntent(fullpath_str);
}

void AAndroidManager::CreateScreenshot(FString fileName) {
	FString full_path = GetDefaultSavedDir() + fileName;
	Log::d("Creating screenshot to path '", to_string(full_path), "'");
	FScreenshotRequest::RequestScreenshot(full_path, false, false);
}

FString AAndroidManager::GetDefaultSavedDir() {
	return FPaths::ProjectSavedDir();
}

FString AAndroidManager::GetDefaultSavedDirAbsolute() {
	if (IsAndroid()) {
		extern FString GFilePathBase;
		return GFilePathBase + FString("/UE4Game/") +
			FApp::GetProjectName() + FString("/") +
			FApp::GetProjectName() + FString("/Saved/");
	}
	else {
		return FPaths::ConvertRelativePathToFull(FPaths::ProjectSavedDir());
	}
}

void AAndroidManager::DeleteTransientTexture(UTexture2D *texture) {
    if (!texture) {
        Log::v("Failed to delete null texture");
        return;
    }
    if (!texture->IsValidLowLevel()) {
        Log::v("Failed to delete invalid texture");
        return;
    }

	texture->ConditionalBeginDestroy(); //instantly clears UObject out of memory
}

EAndroidOrientation AAndroidManager::GetScreenOrientation() const {
	return (EAndroidOrientation) logic->getScreenOrientation();
}

bool AAndroidManager::IsSensorAvailable(EAndroidSensorType type) const {
    int bit_order = (int) type;
    int mask = 1 << bit_order;
    return (logic->getAvailableSensors() & mask) != 0;
}


AAndroidManager* AAndroidManager::GetAndroidManager() {
    return instance;
}

