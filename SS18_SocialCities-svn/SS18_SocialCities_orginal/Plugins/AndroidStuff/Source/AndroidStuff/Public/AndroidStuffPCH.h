#pragma once

// Some copyright should be here...

#pragma once

#ifdef _WIN32
#pragma warning( disable : 4459 )
#endif

#include "Macros.h"
#include "Image.h"
#include "Logger.h"
#include "DataReader.h"
#include "Timer.h"
#include "Values.h"
#include "Plotter.h"

#if defined(__APPLE__)
#import "TargetConditionals.h"
#endif

using Markers::Image;
using Markers::TimedImage;
using Markers::Timer;
using Markers::TimeVal;
using Markers::Color;
using Markers::Colors;
using Markers::DelayLogger;
using Markers::Values;
using Markers::Log;
using Markers::Plotter;
using Markers::SensorData;

using Markers::Vec2i;
using Markers::Vec3i;

using Markers::Vec2;
using Markers::Vec3;
using Markers::Vec4;

using Markers::Vec;
using Markers::Mat;

using Markers::Mat22;
using Markers::Mat33;

using Markers::Aff2;
using Markers::Aff3;

using Markers::lock_guard;
using Markers::str;

using std::vector;
using std::array;
using std::string;
using std::map;
using std::list;

using Functor = std::function<void()>;
