#pragma once

#include <array>
#include <mutex>
#include "CameraImage.h"

struct Camera : public Markers::Repository<CameraImage> {
private:
	int width;
	int height;
	int y_pitch;
	int uv_pitch;
	CameraAPI camera_api;

public:
	Camera();

	Ptr resize(int width, int height, int y_pitch, int uv_pitch, CameraAPI camera_api, bool &resized);

	int getWidth() const;

	int getHeight() const;
};



 