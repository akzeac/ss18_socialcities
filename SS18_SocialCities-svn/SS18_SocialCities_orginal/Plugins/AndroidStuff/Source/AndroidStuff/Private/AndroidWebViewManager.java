package com.epicgames.ue4;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

class WebViewInstance extends HasLogger {
    private final int index;
    private Bitmap bmp;
    private Canvas canvas;
    private WebView view;
    private int width;
    private int height;
    
    private Handler mainThreadHandler = new Handler(Looper.getMainLooper());
    //private int tagOverride = -1;
    
    private static final int DefaultWidth = 640;
    private static final int DefaultHeight = 480;
    
    private static final int DelayLoadRender = 100; //ms
    
    WebViewInstance(final Context context, int index_) {
        index = index_;
        
        view = new WebView(context);
        view.getSettings().setSupportZoom(false);
        view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        view.setWebViewClient(new WebViewClient());
        view.setWebChromeClient(new WebChromeClient());
        //view.setInitialScale(100);
        view.setBackgroundColor(0x00000000);
        view.getSettings().setUseWideViewPort(true);
        
        resize(DefaultWidth, DefaultHeight);
    }
    
    WebViewInstance(final Context context) {
        index = -1;
        
        view = new WebView(context);
        view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        view.setWebViewClient(new WebViewClient());
        view.setWebChromeClient(new WebChromeClient());
        view.setBackgroundColor(0x00000000);
        view.getSettings().setUseWideViewPort(true);
        view.getSettings().setJavaScriptEnabled(true);
        view.addJavascriptInterface(this, "UE");
        resize(DefaultWidth, DefaultHeight);
    }
    
    private void resize(int width_, int height_) {
        width = width_;
        height = height_;
        view.layout(0, 0, width, height);
        bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bmp);
    }
    
    void setURL(String url) {
        Log.v("Browser " + index + ": stopping!");
        view.stopLoading();
        /*
         view.clearHistory();
         view.clearFormData();
         view.clearCache(true);
         view.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
         */
        if (url.isEmpty()) {
            Log.v("Browser " + index + ": reloading!");
            view.reload();
        } else {
            Log.v("Browser " + index + ": loading url '" + url + "'.");
            view.loadUrl(url);
            
        }
    }
    
    void setURLAndRender(final Activity activity, String url, final int width_,
                         final int height_, final ByteBuffer buffer, final int tag) {
        //tagOverride = -1;
        view.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, final String url_) {
                Log.v("Serial Browser: finished loading page.");
                mainThreadHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.v("Serial Browser: rendering '"  + url_ + "' with tag " + tag + " with size " + width_ + "x" + height_);
                        //if (tag != tagOverride) {
                        //    Log.e("Serial Browser: Incorrect tag! Expected " + tag + " but got " + tagOverride);
                        //}
                        render(width_, height_, buffer, tag);
                    }
                }, DelayLoadRender);
            }
        });
        setURL(url);
    }
    
    private void ensureSize(int width_, int height_) {
        if (width != width_ || height != height_) {
            resize(width_, height_);
        }
    }
    
    void render(int width_, int height_, ByteBuffer buffer, int tag) {
        ensureSize(width_, height_);
        canvas.drawColor(0, PorterDuff.Mode.CLEAR);
        view.draw(canvas);
        
        /*
         int color = Color.BLACK;
         switch(tag) {
         case 0: color = Color.BLUE; break;
         case 1: color = Color.GREEN; break;
         case 2: color = Color.RED; break;
         }
         Paint paint = new Paint();
         paint.setColor(color);
         canvas.drawRect(new Rect(0, height - 50, width-1, height-1),
         paint);
         */
        bmp.copyPixelsToBuffer(buffer);
        nativeProcessBrowser(index, tag);
    }
    
    private native void nativeProcessBrowser(int index, int tag);
    
    protected native void nativeProcessCallback(String property, String value);
    
    @JavascriptInterface
    public void postMode(String mode) {
        /*
         Log.v("UE.postMode() provided argument '" + mode + "'.");
         if (mode.equals("c")) {
         tagOverride = 0;
         } else if (mode.equals("s")) {
         tagOverride = 1;
         } else if (mode.equals("f")) {
         tagOverride = 2;
         } else {
         tagOverride = -1;
         }
         */
    }
    
}

class JSInstance extends HasLogger {
    private WebView view;
    
    static class JSInterface {
        private float volume = 1.0f;
        private MediaPlayer player;

        @JavascriptInterface
        public void setVolume(float new_volume) {
            if (new_volume < 0 || new_volume > 1) {
                Log.e("UE.setVolume: Invalid parameter");
            } else {
                volume = new_volume;
            }
        }
        
        @JavascriptInterface
        public void playURL(String url, boolean loop) {
            try {
                if (player != null) {
                    player.release();
                    player = null;
                }
                player = new MediaPlayer();
                player.setVolume(volume, volume);
                player.setLooping(loop);
                player.setDataSource(url);
                player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.start();
                    }
                });
                player.prepareAsync();
            } catch (IllegalArgumentException e) {
                Log.e("IllegalArgumentException from UE.playSoundURL:");
                Log.e(e.getMessage());
            } catch (IllegalStateException e) {
                Log.e("IllegalStateException from UE.playSoundURL:");
                Log.e(e.getMessage());
            } catch (IOException e) {
                Log.e("IOException from UE.playSoundURL:");
                Log.e(e.getMessage());
            }
        }
        
        @JavascriptInterface
        public void stopPlaying() {
            player.stop();
            player.release();
            player = null;
        }
        
        
        public void setProperty(String property, String value) {
            
        }
    }
    
    JSInstance(final Activity context) {
        view = new WebView(context);
        view.setWebViewClient(new WebViewClient());
        view.setWebChromeClient(new WebChromeClient());
        view.getSettings().setSupportZoom(false);
        view.getSettings().setJavaScriptEnabled(true);
        view.getSettings().setMediaPlaybackRequiresUserGesture(false);
        view.addJavascriptInterface(new JSInterface(), "UE");
        
        
    }
    
    void setURL(String url) {
        Log.v("JS Browser: loading url '" + url + "'.");
        view.loadUrl(url);
    }
}


public class AndroidWebViewManager extends HasLogger {
    private static final int Size = 2;
    private ArrayList<WebViewInstance> parallelViews;
    private WebViewInstance[] serialViews = new WebViewInstance[3];
    private JSInstance jsView;
    private Activity activity;
    
    AndroidWebViewManager(Activity activity) {
        this.activity = activity;
        
        parallelViews = new ArrayList<>();
        for (int i = 0; i < Size; i++) {
            parallelViews.add(null);
        }
    }
    
    public void render(final int index, final int width, final int height, final ByteBuffer buffer) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                WebViewInstance view = parallelViews.get(index);
                if (view == null) {
                    view = new WebViewInstance(activity, index);
                    parallelViews.set(index, view);
                }
                view.render(width, height, buffer, 0);
            }
        });
    }
    
    public void setURL(final int index, final String url) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                WebViewInstance view = parallelViews.get(index);
                if (view == null) {
                    view = new WebViewInstance(activity, index);
                    Log.d("AndroidWebViewManager: Initializing parallelView " + index + ".");
                    parallelViews.set(index, view);
                }
                view.setURL(url);
            }
        });
    }
    
    public void setURLAndRender(final String url, final int width, final int height, final ByteBuffer buffer, final int tag) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (serialViews[tag] == null) {
                    serialViews[tag] = new WebViewInstance(activity);
                    Log.d("AndroidWebViewManager: Initializing serialView " + tag);
                    
                    if (tag == 0) {
                        // in /Epic Games/UE_4.19/Engine/Build/Android/Java/src/com/epicgames/ue4/WebViewControl.java
                        /*
                        WebViewControl.jsCallback = new WebViewControl.JSCallback () {
                            public void OnJsMessage(String text) {
                                serialViews[0].nativeProcessCallback(text, "");
                            }
                        };
                        Log.d("AndroidWebViewManager: Setting jsCallback");
                         */
                    }
                }
                serialViews[tag].setURLAndRender(activity, url, width, height, buffer, tag);
            }        });
    }
    
    public void setURLAsControl(final String url) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (jsView == null) {
                    jsView = new JSInstance(activity);
                    Log.d("AndroidWebViewManager: Initializing jsView.");
                }
                jsView.setURL(url);
            }
        });
        
    }
}

