package com.epicgames.ue4;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.Display;
import android.view.WindowManager;

import java.nio.ByteBuffer;

public class AndroidManager extends HasLogger {
    public enum State {
        ACTIVE(0),
        STOPPED(1),
        PAUSED(2),
        DESTROY(3);
        
        int id;
        State(int id_) {
            id = id_;
        }
    }
    private State state;
    
    private AndroidCameraManager camera;
    private AndroidWebViewManager webViews;
    private IntentManager intent;
    private MotionManager sensors;
    private Activity activity;
    
    public AndroidManager(Activity activity_) {
        activity = activity_;
        webViews = new AndroidWebViewManager(activity);
        camera = null;
        sensors = new MotionManager(activity);
        intent = new IntentManager(activity);
        state = State.ACTIVE;
        Log.d("Setting activity state to ACTIVE.");
    }
    
    public void setBrowserURL(int i, String url) {
        webViews.setURL(i, url);
    }
    
    public void renderBrowser(int i, int width, int height, ByteBuffer buffer) {
        webViews.render(i, width, height, buffer);
    }
    
    public void setBrowserURLAndRender(String url, int width, int height, ByteBuffer buffer, int tag) {
        webViews.setURLAndRender(url, width, height, buffer, tag);
    }
    
    public void setBrowserURLAsControl(String url) {
        webViews.setURLAsControl(url);
    }
    
    public boolean setupCamera(int api, int facing) {
        if (api == 1) {
            camera = new CameraManagerAPI1(activity);
        } else if (api == 2) {
            camera = new CameraManagerAPI2(activity);
        } else {
            Log.d("Unknown camera API!");
            return false;
        }
        return camera.setup(facing);
    }
    
    public int[] setCameraParameters(int width, int height, float exposure_time) {
        return camera.setCameraParameters(width, height, exposure_time);
    }
    
    public void openCamera() {
        camera.open();
    }
    
    public void releaseCamera() {
        //camera.close(); // FIXME
    }
    
    public void onResume() {
        state = State.ACTIVE;
        Log.d("Setting state to ACTIVE.");
    }
    
    public void onStop() {
        state = State.STOPPED;
        Log.d("Setting state to STOPPED.");
    }
    
    public void onPause() {
        state = State.PAUSED;
        Log.d("Setting state to PAUSED.");
    }
    
    public void onDestroy() {
        state = State.DESTROY;
        Log.d("Setting state to DESTROY.");
    }
    
    public int getActivityState() {
        return state.id;
    }
    
    public void launchShareImageIntent(String filename) {
        intent.launchShareImage(filename);
    }
    
    public void startSensors() {
        sensors.start();
    }
    
    public int getScreenOrientation() {
        Display display = ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        return display.getRotation();
    }
    
    public int getAvailableSensors() {
        return sensors.getAvailableSensors();
    }
    
}


class HasLogger {
    static class MyLogger {
        private final static String mTag = "LogProject";
        
        public void d(String Message) {
            android.util.Log.d(mTag, "Java: " + Message);
        }
        
        public void w(String Message) {
            android.util.Log.w(mTag, "Java: " + Message);
        }
        
        public void e(String Message) {
            android.util.Log.e(mTag, "Java: " + Message);
        }
        
        public void e(Exception e) {
            android.util.Log.e(mTag, "Java: ", e);
        }
        
        public void v(String Message) {
            android.util.Log.v(mTag, "Java: " + Message);
        }
    }
    
    protected static MyLogger Log = new MyLogger();
}

class IntentManager extends HasLogger {
    private HandlerThread thread;
    private Handler handler;
    private Activity activity;
    
    IntentManager(Activity activity) {
        thread = new HandlerThread("proj:intent");
        thread.start();
        handler = new Handler(thread.getLooper());
        this.activity = activity;
    }
    
    public void launchShareImage(final String filename) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent sendIntent = new Intent();
                    sendIntent.setType("image/png");
                    sendIntent.setAction(Intent.ACTION_SEND);
                    Log.d("Starting Share Image intent for file '" + filename + "'");
                    sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + filename));
                    activity.startActivity(Intent.createChooser(sendIntent, "Share Image"));
                } catch(Exception e) {
                    Log.e("Failed to launch intent!");
                    Log.e(e);
                }
            }
        });
    }
}


class MotionManager extends HasLogger implements SensorEventListener {
    private HandlerThread thread;
    private Handler handler;
    private Activity activity;
    
    private final int[] SensorTypes = new int[] {
    Sensor.TYPE_ACCELEROMETER,
    Sensor.TYPE_LINEAR_ACCELERATION,
    Sensor.TYPE_GAME_ROTATION_VECTOR,
    Sensor.TYPE_GYROSCOPE
    };
    
    MotionManager(Activity activity) {
        thread = new HandlerThread("proj:sensor");
        thread.start();
        handler = new Handler(thread.getLooper());
        this.activity = activity;
    }
    
    int getAvailableSensors() {
        SensorManager mSensorManager =
        (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
        int result = 0;
        for (int i = 0; i < SensorTypes.length; i++) {
            if (mSensorManager.getDefaultSensor(SensorTypes[i]) != null) {
                result += (1 << i);
            }
        }
        return result;
    }
    
    void start() {
        Log.d("Starting sensors.");
        handler.post(new Runnable() {
            @Override
            public void run() {
                SensorManager mSensorManager =
                (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
                Sensor sensor;
                
                for (int sensorType : SensorTypes) {
                    sensor = mSensorManager.getDefaultSensor(sensorType);
                    if (sensor != null) {
                        mSensorManager.registerListener(MotionManager.this,
                                                        sensor, SensorManager.SENSOR_DELAY_GAME);
                    }
                }
            }
        });
    }
    
    @Override
    public void onSensorChanged(SensorEvent event) {
        //((MainActivity)activity).onSensorDataArrived(event.sensor.getType(), event.values);
        nativeOnSensorDataArrived(event.sensor.getType(), event.values);
    }
    
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }
    
    private native void nativeOnSensorDataArrived(int type, float[] array);
}


