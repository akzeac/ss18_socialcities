#include "AndroidStuffPCH.h"
#include "CameraImage.h"
#include "ImageUtils.h"

using Markers::ImageUtils;

CameraImage::CameraImage() :
	width(0), height(0),
	y_pitch(0), uv_pitch(0),
	raw_ptr(nullptr),
	raw_size(0),
	timestamp(-1),
	camera_api(CameraAPI::Invalid) {}


static int getBufferSize(int width, int height, int y_pitch, int uv_pitch, CameraAPI camera_api) {
	switch (camera_api) {
	case CameraAPI::V1:
		return width * height * 3 / 2;
	case CameraAPI::V2:
		return y_pitch * height + uv_pitch * height;
	default:
		return 0;
	}
}

CameraImage::CameraImage(int width_, int height_, int y_pitch_, int uv_pitch_, CameraAPI camera_api_) :
	raw_yuv(getBufferSize(width_, height_, y_pitch_, uv_pitch_, camera_api_)),
	y(width_, height_, 1),
	u(width_ / 2, height_ / 2, 1),
	v(width_ / 2, height_ / 2, 1),
	width(width_), height(height_),
	y_pitch(y_pitch_), uv_pitch(uv_pitch_),
	raw_ptr(raw_yuv.data()),
	raw_size(raw_yuv.size()),
	timestamp(-1),
	camera_api(camera_api_) {}


void CameraImage::unpackY() {
	if (y_pitch != width) {
		ImageUtils::SplitY(raw_ptr, y.ptr, width, height, y_pitch);
	}
	else {
		memcpy(y.ptr, raw_ptr, y.size);
	}
}

void CameraImage::unpackUV() {
	const uint8_t *uv_src = raw_ptr + y_pitch * height;
	switch(camera_api) {
	case CameraAPI::V1:
		ImageUtils::SplitUV_Interleaved(uv_src, u.ptr, v.ptr, width * height / 4);
		break;
	case CameraAPI::V2:
		// FIXME: U and V are switched here
		if (uv_pitch < width) { // u,v are 1 bpp
			ImageUtils::SplitUV_1bpp(uv_src, v.ptr, u.ptr, width, height, uv_pitch);
		}
		else { // u,v are 2 bpp
			ImageUtils::SplitUV_2bpp(uv_src, v.ptr, u.ptr, width, height, uv_pitch);
		}
		break;
	default:
		break;
	}
}


void CameraImage::toColorImage(Image &rgba) const {
	if (rgba.width != width || rgba.height != height) {
		Log::e("Image size does not match! Expected ",
			width, "x", height, ", got ", rgba.width, "x", rgba.height);
		return;
	}
	switch (camera_api) {
	case CameraAPI::V1: 
	{
		const uint8_t *uv_src = raw_ptr + y_pitch * height;
		ImageUtils::Convert_YUV_RGB_1(raw_ptr, uv_src, rgba.ptr, width, height);
		break;
	}
	case CameraAPI::V2:
	{
		const uint8_t *u_src = raw_ptr + y_pitch * height;
		const uint8_t *v_src = u_src + height * uv_pitch / 2;
		ImageUtils::Convert_YUV_RGB_2(raw_ptr, u_src, v_src, rgba.ptr,
			width, height, y_pitch, uv_pitch);
		break;
	}
	default:
		break;
	}
}

void CameraImage::toColorImageHalf(Image &rgba) const {
	if (rgba.width != width / 2 || rgba.height != height / 2) {
		Log::e("Image size does not match! Expected ",
			width / 2, "x", height / 2, ", got ", rgba.width, "x", rgba.height);
		return;
	}
	switch (camera_api) {
	case CameraAPI::V1:
	{
		Log::w("CameraImage::toColorImageHalf not implemented for API 1");
		break;
	}
	case CameraAPI::V2:
	{
		const uint8_t *u_src = raw_ptr + y_pitch * height;
		const uint8_t *v_src = u_src + height * uv_pitch / 2;
		ImageUtils::Convert_YUV_RGB_Half(raw_ptr, u_src, v_src, rgba.ptr,
			width, height, y_pitch, uv_pitch);
		break;
	}
	default:
		break;
	}
}
