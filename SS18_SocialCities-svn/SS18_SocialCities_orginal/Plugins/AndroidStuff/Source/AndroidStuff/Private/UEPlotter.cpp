#include "AndroidStuffPCH.h"
#include "UEPlotter.h"
#include "Logic.h"
#include "DrawDebugHelpers.h"

using Markers::CloudToWorld;

static void drawLine(UWorld *world, const Vec3 &lA, const Vec3 &lB, Color lc, float thick) {
	FVector A = { lA(0), lA(1), lA(2) };
	FVector B = { lB(0), lB(1), lB(2) };
	FColor c = { lc(0), lc(1) , lc(2) , lc(3) };
	DrawDebugLine(world, A, B, c, false, -1.0f, 0, thick);
}

void UEPlotter::plotGlobal(UWorld *world, float thick) {
	for (const Line3 &line : lines_3) {
		Vec3 A = Logic::TransformPointToUE(line.A);
		Vec3 B = Logic::TransformPointToUE(line.B);
		drawLine(world, A, B, line.c, thick);
	}
}

void UEPlotter::plotLocal(UWorld *world, const Aff2 &Iinv, const Aff3 &E, float z, float thick) {
	for (const Line2 &line : lines_2) {
		Vec3 A = E * (CloudToWorld * unproject(line.A, Iinv, z));
		Vec3 B = E * (CloudToWorld * unproject(line.B, Iinv, z));

		A = Logic::TransformPointToUE(A);
		B = Logic::TransformPointToUE(B);

		drawLine(world, A, B, line.c, thick);
	}
}

void UEPlotter::plotToUE(UWorld *world, const Aff2 &Iinv, const Aff3 &E, float z, float thick) {
	lock_guard lock(mutex);
	plotGlobal(world, thick);
	plotLocal(world, Iinv, E, z, thick);
}


void UEPlotter::append(const UEPlotter &plotter) {
	lock_guard lock(mutex);
	lines_2.reserve(lines_2.size() + plotter.lines_2.size());
	lines_2.insert(lines_2.end(), plotter.lines_2.begin(), plotter.lines_2.end());
	lines_3.reserve(lines_3.size() + plotter.lines_3.size());
	lines_3.insert(lines_3.end(), plotter.lines_3.begin(), plotter.lines_3.end());
}
