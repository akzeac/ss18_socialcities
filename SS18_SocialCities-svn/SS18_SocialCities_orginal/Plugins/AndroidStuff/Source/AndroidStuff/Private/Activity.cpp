#include "AndroidStuffPCH.h"
#include "Activity.h"
#include "AndroidManager.h"

#include "IOSHelper.h"

#ifdef __ANDROID__ 
#include "Android/AndroidJNI.h"
#include "Android/AndroidApplication.h"

static JNIEnv* ENV = nullptr;

static jmethodID jni_setupCamera;
static jmethodID jni_setCameraParameters;
static jmethodID jni_openCamera;
static jmethodID jni_closeCamera;
static jmethodID jni_getActivityState;
static jmethodID jni_startSensors;
static jmethodID jni_renderBrowser;
static jmethodID jni_setBrowserURL;
static jmethodID jni_setBrowserURLAndRender;
static jmethodID jni_startIntent;
static jmethodID jni_getOrientation;
static jmethodID jni_getAvailableSensors;
static jmethodID jni_setBrowserURLAsControl;

static jmethodID findMethod(const char *name, const char *sig) {
	Log::d("Loading method '", name, "' with signature '", sig, "'.");
	return FJavaWrapper::FindMethod(ENV, FJavaWrapper::GameActivityClassID, name, sig, false);
}

static void loadJNI() {
	ENV = FAndroidApplication::GetJavaEnv();
	jni_setupCamera = findMethod("AndroidThunkJava_setupCamera", "(II)Z");
	jni_setCameraParameters = findMethod("AndroidThunkJava_setCameraParameters", "(IIF)[I");
	jni_openCamera = findMethod("AndroidThunkJava_openCamera", "()V");
	jni_closeCamera = findMethod("AndroidThunkJava_closeCamera", "()V");
	jni_getActivityState = findMethod("AndroidThunkJava_getActivityState", "()I");
	jni_startSensors = findMethod("AndroidThunkJava_startSensors", "()V");
	jni_renderBrowser = findMethod("AndroidThunkJava_renderBrowser", "(IIILjava/nio/ByteBuffer;)V");
	jni_setBrowserURL = findMethod("AndroidThunkJava_setBrowserURL", "(ILjava/lang/String;)V");
    jni_setBrowserURLAndRender = findMethod("AndroidThunkJava_setBrowserURLAndRender", "(Ljava/lang/String;IILjava/nio/ByteBuffer;I)V");
    jni_setBrowserURLAsControl = findMethod("AndroidThunkJava_setBrowserURLAsControl", "(Ljava/lang/String;)V");
    jni_startIntent = findMethod("AndroidThunkJava_startIntent", "(Ljava/lang/String;)V");
	jni_getOrientation = findMethod("AndroidThunkJava_getScreenRotation", "()I");
    jni_getAvailableSensors = findMethod("AndroidThunkJava_getAvailableSensors", "()I");

}

extern "C"
void Java_com_epicgames_ue4_MotionManager_nativeOnSensorDataArrived(
	JNIEnv* LocalJNIEnv, jobject /* this */,  
	jint type, jfloatArray array) 
{
	int length = LocalJNIEnv->GetArrayLength(array);
	jfloat *ptr = LocalJNIEnv->GetFloatArrayElements(array, nullptr);
	AndroidActivity::OnSensorDataArrived(type, ptr, length);
	LocalJNIEnv->ReleaseFloatArrayElements(array, ptr, JNI_ABORT);
}

extern "C"
void Java_com_epicgames_ue4_CameraManagerAPI1_nativeUpdateTexture(
	JNIEnv* LocalJNIEnv, jobject,
	jbyteArray array, jint width, jint height)
{
	CameraImage::Ptr buffer =
		AndroidActivity::GetCameraImage(width, height, width, width, CameraAPI::V1);
	if (buffer == nullptr) {
		return;
	}
	uint8_t *ptr_dst = buffer->raw_ptr;
	int array_size = LocalJNIEnv->GetArrayLength(array);
	LocalJNIEnv->GetByteArrayRegion(array, 0, array_size, (jbyte*)ptr_dst);

	AndroidActivity::OnCameraImageArrived(buffer);
}

extern "C"
void Java_com_epicgames_ue4_CameraManagerAPI2_nativeUpdateTexture(
		JNIEnv* LocalJNIEnv, jobject,
		jobject y_buffer, jobject u_buffer, jobject v_buffer,
		jintArray temp_buffer) {

	int temp[5];
	LocalJNIEnv->GetIntArrayRegion(temp_buffer, 0, 5, temp);
	int width = temp[0];
	int height = temp[1];
	int y_pitch = temp[2];
	int u_pitch = temp[3];
	int v_pitch = temp[4];

	CameraImage::Ptr buffer = 
		AndroidActivity::GetCameraImage(width, height, y_pitch, u_pitch, CameraAPI::V2);
	if (buffer == nullptr) {
		return;
	}

	jlong y_size = LocalJNIEnv->GetDirectBufferCapacity(y_buffer);
	jlong u_size = LocalJNIEnv->GetDirectBufferCapacity(u_buffer);
	jlong v_size = LocalJNIEnv->GetDirectBufferCapacity(v_buffer);
	jlong y_size_exp = y_pitch * height;
	jlong u_size_exp = u_pitch * height / 2;
	jlong v_size_exp = v_pitch * height / 2;

	uint8_t *y_ptr_dst = buffer->raw_ptr;
	uint8_t *u_ptr_dst = y_ptr_dst + y_size_exp;
	uint8_t *v_ptr_dst = u_ptr_dst + u_size_exp;
	const void *y_ptr_src = LocalJNIEnv->GetDirectBufferAddress(y_buffer);
	const void *u_ptr_src = LocalJNIEnv->GetDirectBufferAddress(u_buffer);
	const void *v_ptr_src = LocalJNIEnv->GetDirectBufferAddress(v_buffer);
	memcpy(y_ptr_dst, y_ptr_src, std::min(y_size, y_size_exp));
	memcpy(u_ptr_dst, u_ptr_src, std::min(u_size, u_size_exp));
	memcpy(v_ptr_dst, v_ptr_src, std::min(v_size, v_size_exp));

	AndroidActivity::OnCameraImageArrived(buffer);
}

extern "C"
void Java_com_epicgames_ue4_WebViewInstance_nativeProcessBrowser(JNIEnv*, jobject, jint index, jint tag) {
	AndroidActivity::OnBrowserImageArrived(index, tag);
}

extern "C"
void Java_com_epicgames_ue4_WebViewInstance_nativeProcessCallback(JNIEnv *env, jobject, jstring property, jstring value) {
    const char* property_ptr = env->GetStringUTFChars(property, nullptr);
    std::string property_str = property_ptr;
    env->ReleaseStringUTFChars(property, property_ptr);
    
    const char* value_ptr = env->GetStringUTFChars(value, nullptr);
    std::string value_str = value_ptr;
    env->ReleaseStringUTFChars(value, value_ptr);

    AndroidActivity::OnBrowserPropertyArrived(property_str, value_str);
}
#endif

ActivityListener *Activity::Instance = nullptr;

AndroidActivity::AndroidActivity(ActivityListener *Instance_) {
	Instance = Instance_;
#if PLATFORM_ANDROID
	loadJNI();
#endif
}

AndroidActivity::~AndroidActivity() {
	Instance = nullptr;
}

void Activity::OnCameraImageArrived(const CameraImage::Ptr &buffer) {
	if (Instance != nullptr) {
		Instance->onCameraImageArrived(buffer);
	}
}

void Activity::OnBrowserImageArrived(int index, int tag) {
	if (Instance != nullptr) {
		Instance->onBrowserImageArrived(index, tag);
	}
}

void Activity::OnBrowserPropertyArrived(const std::string &property, const std::string &value) {
    if (Instance != nullptr) {
        Instance->onBrowserPropertyArrived(property, value);
    }
}

bool AndroidActivity::setupCamera(CameraAPI api, CameraFacing facing) const {
	bool ret = false;
	int api_id = (int)api;
	int facing_id = (int)facing;

#if PLATFORM_ANDROID
	ret = FJavaWrapper::CallBooleanMethod(ENV, FJavaWrapper::GameActivityThis, 
		jni_setupCamera, api_id, facing_id);
#endif
	return ret;
}

void AndroidActivity::openCamera() {
#if PLATFORM_ANDROID
	FJavaWrapper::CallVoidMethod(ENV, FJavaWrapper::GameActivityThis, jni_openCamera);
#endif
}

void AndroidActivity::closeCamera() {
#if PLATFORM_ANDROID
	FJavaWrapper::CallVoidMethod(ENV, FJavaWrapper::GameActivityThis, jni_closeCamera);
#endif
}

Vec2i AndroidActivity::setCameraParameters(int width, int height, float exposure_time_ms) {
	Vec2i ret = { 0, 0 };
#if PLATFORM_ANDROID
	jintArray array = (jintArray)
		FJavaWrapper::CallObjectMethod(ENV, 
			FJavaWrapper::GameActivityThis, 
			jni_setCameraParameters,
			(jint) width, (jint) height, (jfloat) exposure_time_ms);
	jint *ptr = ENV->GetIntArrayElements(array, nullptr);
	ret = { ptr[0], ptr[1] };
	ENV->ReleaseIntArrayElements(array, ptr, JNI_ABORT);
#endif
	return ret;
}

uint8_t AndroidActivity::getActivityState() const {
	int ret = 0;
#if PLATFORM_ANDROID
	ret = FJavaWrapper::CallIntMethod(ENV, FJavaWrapper::GameActivityThis, jni_getActivityState);
#endif
	return ret;
}

CameraImage::Ptr Activity::GetCameraImage(int width, int height,
		int y_pitch, int uv_pitch, CameraAPI camera_api) {
	return (Instance != nullptr) ?
		Instance->getCameraImage(width, height, y_pitch, uv_pitch, camera_api) :
		nullptr;
}

void Activity::OnSensorDataArrived(int type, const float *data, int size) {
	if (Instance != nullptr) {
		SensorData sd = { 
			(SensorData::Type)type, 
			Timer::getTicks(), 
			{ data[0],data[1], data[2], data[3] } 
		};
		Instance->onSensorDataArrived(sd);
	}
}

void AndroidActivity::startSensors() {
#if PLATFORM_ANDROID
	FJavaWrapper::CallVoidMethod(ENV, FJavaWrapper::GameActivityThis, jni_startSensors);
#endif
}

void AndroidActivity::setBrowserURL(int index, const std::string &url) {
#if PLATFORM_ANDROID
	jstring url_jni = ENV->NewStringUTF(url.c_str());
	FJavaWrapper::CallVoidMethod(ENV, FJavaWrapper::GameActivityThis,
		jni_setBrowserURL, (jint)index, url_jni);
	ENV->DeleteLocalRef(url_jni);
#endif
}

void AndroidActivity::renderBrowser(int index, int width, int height, const BrowserImage::Ptr &image) {
#if PLATFORM_ANDROID
	jobject bytebuffer = ENV->NewDirectByteBuffer(image->ptr, image->size);
	FJavaWrapper::CallVoidMethod(ENV, FJavaWrapper::GameActivityThis,
		jni_renderBrowser, (jint)index, (jint)width, (jint)height, bytebuffer);
	ENV->DeleteLocalRef(bytebuffer);
#endif
}

void AndroidActivity::setBrowserURLAndRender(const std::string &url, int width, int height, const BrowserImage::Ptr &image, int tag) {
#if PLATFORM_ANDROID
    jstring url_jni = ENV->NewStringUTF(url.c_str());
    jobject bytebuffer = ENV->NewDirectByteBuffer(image->ptr, image->size);
    FJavaWrapper::CallVoidMethod(ENV, FJavaWrapper::GameActivityThis,
                                 jni_setBrowserURLAndRender, url_jni, width, height, bytebuffer, tag);
    ENV->DeleteLocalRef(bytebuffer);
    ENV->DeleteLocalRef(url_jni);
#endif
}

void AndroidActivity::startIntent(const std::string &filename) {
#if PLATFORM_ANDROID
	jstring name_jni = ENV->NewStringUTF(filename.c_str());
	FJavaWrapper::CallVoidMethod(ENV, FJavaWrapper::GameActivityThis, jni_startIntent, name_jni);
	ENV->DeleteLocalRef(name_jni);
#endif
}

int AndroidActivity::getScreenOrientation() const {
	int ret = 0;
#if PLATFORM_ANDROID
	ret = FJavaWrapper::CallIntMethod(ENV, FJavaWrapper::GameActivityThis, jni_getOrientation);
#endif
	return ret;
}

int AndroidActivity::getAvailableSensors() const {
    int ret = 0;
#if PLATFORM_ANDROID
    ret = FJavaWrapper::CallIntMethod(ENV, FJavaWrapper::GameActivityThis, jni_getAvailableSensors);
#endif
    return ret;
}

void AndroidActivity::setBrowserURLAsControl(const std::string &url) {
#if PLATFORM_ANDROID
    jstring url_jni = ENV->NewStringUTF(url.c_str());
    FJavaWrapper::CallVoidMethod(ENV, FJavaWrapper::GameActivityThis, jni_setBrowserURLAsControl, url_jni);
    ENV->DeleteLocalRef(url_jni);
#endif

}




/****************************************************************************************/

IOSActivity::IOSActivity(ActivityListener *Instance_) {
    Instance = Instance_;
#if PLATFORM_ANDROID
    loadJNI();
#endif
}

IOSActivity::~IOSActivity() {
    Instance = nullptr;
}

bool IOSActivity::setupCamera(CameraAPI api, CameraFacing facing) const {
    Log::e("setupCamera not implemented!");
    return false;
}

Vec2i IOSActivity::setCameraParameters(int width, int height, float exposure_time_ms) {
    Log::e("setCameraParameters not implemented!");
    return { 0, 0 };
}

void IOSActivity::openCamera() {
    Log::e("openCamera not implemented!");
}

void IOSActivity::closeCamera() {
    Log::e("closeCamera not implemented!");
}

uint8_t IOSActivity::getActivityState() const {
    Log::e("getActivityState not implemented!");
    return 0;
}

void IOSActivity::startSensors() {
    Log::e("startSensors not implemented!");
}

void IOSActivity::setBrowserURL(int index, const std::string &url) {
    Log::e("setBrowserURL not implemented!");
}

void IOSActivity::renderBrowser(int index, int width, int height, const BrowserImage::Ptr &image) {
    Log::e("renderBrowser not implemented!");
}


static void _OnBrowserImageArrived(IOS_CallbackBuffer buffer) {
    AndroidActivity::OnBrowserImageArrived(-1, buffer.tag);
}

void IOSActivity::setBrowserURLAndRender(const std::string &url, int width, int height, const BrowserImage::Ptr &image, int tag) {
    Log::d("setBrowserURLAndRender: Got in!");
    IOS_initWebView(width, height, tag);
    IOS_setCallback(_OnBrowserImageArrived, image->ptr, tag);
    IOS_loadURL(url.c_str(), tag);
}

void IOSActivity::setBrowserURLAsControl(const std::string &url) {
    Log::e("setBrowserURLAsControl not implemented!");
}

void IOSActivity::startIntent(const string &filename) {
    Log::e("startIntent not implemented!");
}

int IOSActivity::getScreenOrientation() const {
    Log::e("getScreenOrientation not implemented!");
    return 0;
}

int IOSActivity::getAvailableSensors() const {
    Log::e("getAvailableSensors not implemented!");
    return 0;
}








