#include "AndroidStuffPCH.h"
#include "UELogger.h"
#include "Timer.h"
#include "Engine.h"

#if PLATFORM_ANDROID
#include "Android/AndroidJNI.h"
#include "Android/AndroidApplication.h"
#endif

DECLARE_LOG_CATEGORY_EXTERN(LogProject, Log, All);
DEFINE_LOG_CATEGORY(LogProject);

void UEScreenLog::s(const std::string &msg, const Color &color) {
	GEngine->AddOnScreenDebugMessage(-1, 10.f, { color[0], color[1], color[2] }, UTF8_TO_TCHAR(msg.c_str()));
}

const char *UEConsoleLog::Tag = "LogProject";

void UEConsoleLog::d(const std::string &msg) {
	UE_LOG(LogProject, Display, TEXT("C++: (%lld) %s"), Timer::getTicks(), 
		*FString(ANSI_TO_TCHAR(msg.c_str())));
}

void UEConsoleLog::e(const std::string &msg) {
	UE_LOG(LogProject, Error, TEXT("C++: (%lld) %s"), Timer::getTicks(),
		*FString(ANSI_TO_TCHAR(msg.c_str())));
}

void UEConsoleLog::w(const std::string &msg) {
	UE_LOG(LogProject, Warning, TEXT("C++: (%lld) %s"), Timer::getTicks(),
		*FString(ANSI_TO_TCHAR(msg.c_str())));
}

void UEConsoleLog::v(const std::string &msg) {
	UE_LOG(LogProject, Display, TEXT("C++: (%lld) %s"), Timer::getTicks(),
		*FString(ANSI_TO_TCHAR(msg.c_str())));
}


const char *AndroidLog::Tag = "LogProject";

void AndroidLog::d(const std::string &msg) {
#if PLATFORM_ANDROID
	__android_log_print(ANDROID_LOG_DEBUG, Tag, "C++: (%lld) %s", (long long)Timer::getTicks(), msg.c_str());
#endif
}

void AndroidLog::e(const std::string &msg) {
#if PLATFORM_ANDROID
	__android_log_print(ANDROID_LOG_ERROR, Tag, "C++: (%lld) %s", (long long)Timer::getTicks(), msg.c_str());
#endif
}

void AndroidLog::w(const std::string &msg) {
#if PLATFORM_ANDROID
	__android_log_print(ANDROID_LOG_WARN, Tag, "C++: (%lld) %s", (long long)Timer::getTicks(), msg.c_str());
#endif
}

void AndroidLog::v(const std::string &msg) {
#if PLATFORM_ANDROID
	__android_log_print(ANDROID_LOG_VERBOSE, Tag, "C++: (%lld) %s", (long long)Timer::getTicks(), msg.c_str());
#endif
}



void IOSLog::d(const std::string &msg) {
    UE_LOG(LogProject, Display, TEXT("C++: (%lld) %s"), Timer::getTicks(),
           *FString(ANSI_TO_TCHAR(msg.c_str())));
    IOS_printd(msg.c_str());
}

void IOSLog::e(const std::string &msg) {
    UE_LOG(LogProject, Error, TEXT("C++: (%lld) %s"), Timer::getTicks(),
           *FString(ANSI_TO_TCHAR(msg.c_str())));
    IOS_printe(msg.c_str());
}

void IOSLog::w(const std::string &msg) {
    UE_LOG(LogProject, Warning, TEXT("C++: (%lld) %s"), Timer::getTicks(),
           *FString(ANSI_TO_TCHAR(msg.c_str())));
    IOS_printw(msg.c_str());
}

void IOSLog::v(const std::string &msg) {
    UE_LOG(LogProject, Display, TEXT("C++: (%lld) %s"), Timer::getTicks(),
           *FString(ANSI_TO_TCHAR(msg.c_str())));
    IOS_printv(msg.c_str());
}

