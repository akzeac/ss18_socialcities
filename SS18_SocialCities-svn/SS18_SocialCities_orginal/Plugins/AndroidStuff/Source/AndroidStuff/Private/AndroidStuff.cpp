// Some copyright should be here...

#include "AndroidStuffPCH.h"
#include "AndroidStuff.h"
#include "AndroidStuffPrivatePCH.h"



#define LOCTEXT_NAMESPACE "FAndroidStuffModule"

void FAndroidStuffModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	
}

void FAndroidStuffModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	
	
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FAndroidStuffModule, AndroidStuff)
